# Real Time Rendering & 3D Games, Semester 2 2014 #

Real Time Rendering & 3D Games is an advanced course focusing on OpenGL offered at RMIT. It aims to build upon the skills and knowledge from its predecessor course (Interactive 3D) using modern OpenGL techniques.  
This repository contains code that was submitted as part of the first, second and third assignment.

### At a glance ###
Written in C and C++ using OpenGL.  
Assignment 1: Procedurally generated mesh using different rendering modes such as **Immediate Mode**, **Vertex Arrays** and **Vertex Buffer Objects**.  
Assignment 2: Extends the first assignment with **programmable shaders**, management of **ModelView matrix** using GLM, experimentation on **Phong vs Blinn-Phong lighting**.  
Assignment 3: An Osmos clone where motes will will undergo absorption behaviour on collision. This assignment is focused on **collision detection techniques** and **physics**.  
** Note: ** Assignment 3 is built on skeleton code given by the lecturer/tutor.

### Contributions ###
**Paul Hoang**  
PaulKhoaHoang {at} gmail.com   

**Geoff Leach/Jesse Archer** (Lecturer/Head tutor)  
gl {at} rmit.edu.au  
jesse.archer {at} rmit.edu.au