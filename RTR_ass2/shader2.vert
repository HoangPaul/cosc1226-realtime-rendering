uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;
uniform mat3 normalMatrix;

uniform float time;
uniform bool is3D;

varying vec4 colour;
varying vec3 normal;
varying vec4 vertex;

void main(void) {
    float A1 = 0.25, k1 = 2.0 * 3.1415, w1 = 0.25;

    vec4 newVert = gl_Vertex;    
    vec3 newNorm = gl_Normal;

    newVert.y = A1 * sin(k1 * gl_Vertex.x + w1 * time);
    if (is3D) {
       newVert.y += A1 * sin(k1 * gl_Vertex.z + w1 * time); 
    }

    newNorm.x = -A1 * k1 * cos(k1 * gl_Vertex.x + w1 * time);
    newNorm.y = 1.0;
    newNorm.z = 0.0;
    if (is3D) {
        newNorm.z = -A1 * k1 * cos(k1 * gl_Vertex.z + w1 * time);
    }
    
    newNorm = normalize(newNorm);

    gl_Position = projectionMatrix * modelViewMatrix * newVert;
    normal = newNorm;
    vertex = newVert;
    colour = gl_Color;
}
