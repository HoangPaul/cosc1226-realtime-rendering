#include "mesh.h"

void calcIndicies(Mesh *mesh, int tess);
void calcGrid(Global g, Mesh *mesh);
void calcFlatGrid(Global g, Mesh *mesh);
void calcIndiciesOneMesh(Mesh*, int);
void uploadDataVBO(Global *g, Mesh *mesh);
void uploadMeshVBO(Global *g, Mesh *mesh);
void uploadDataVBOOneMesh(Global *g, Mesh *mesh);

glm::vec3 blued( 1.0, 0.0, 1.0 );

void initVboData(Global *g) {
    g->VboOneMesh = new GLuint[1];//(GLuint*)calloc(1, sizeof(GLuint));
    g->VboMesh = new GLuint[1];//(GLuint*)calloc(1, sizeof(GLuint));
    
    glGenBuffers(1, g->VboMesh);
    glGenBuffers(1, g->VboOneMesh);
    
}

void allocateMeshSpace(Mesh *mesh, int nIndicies, int nVerts, int tess) {
    if (mesh->nIndicies < nIndicies) {
        if (mesh->tess == 0) {           
            mesh->indicies = new int[nIndicies];//(int*) calloc(nIndicies, sizeof(int));
        } else {
            delete [] mesh->indicies;
            mesh->indicies = new int[nIndicies];//(int*)realloc(mesh->indicies, nIndicies * sizeof(int));
        }
    }
    if (mesh->nVerts < nVerts) {
        if (mesh->tess == 0) {
            mesh->verticies = new Vertex[nVerts];//(Vertex*) calloc(nVerts, sizeof(Vertex));
        } else {
            delete [] mesh->verticies;
            mesh->verticies = new Vertex[nVerts];//(Vertex*)realloc(mesh->verticies, nVerts * sizeof(Vertex));
        }
    }
    mesh->nIndicies = nIndicies;
    mesh->nVerts = nVerts;
    mesh->tess = tess;

    
}

void allocateVboSpace(Global *g, int tess) {
    if (g->nVboIds < tess) {
        if (g->nVboIds == 0) {
            g->VboIds = new GLuint[tess];//(GLuint*)calloc(tess, sizeof(GLuint));
        } else {
            delete [] g->VboIds;
            g->VboIds = new GLuint[tess];//(GLuint*)realloc(g->VboIds, tess * sizeof(GLuint));
        }
    }
    if (g->nVboIds != tess) {
        if (g->nVboIds != 0) {
            glDeleteBuffers(g->nVboIds, g->VboIds);
        }
        glGenBuffers(tess, g->VboIds);
    }
    g->nVboIds = tess;
}

void allocateSpace(Global *g, Mesh *mesh) {
    int tess = g->tess;
    int nIndicies;
    int nVerts;

    nVerts = (tess+1) * (tess+1);
    nIndicies = 2 * (tess+1) * tess;
    
    allocateMeshSpace(mesh, nIndicies, nVerts, tess);
    if (g->renderType == Vbo || g->renderType == VboOneMesh) {
        allocateVboSpace(g, tess);
    }
}

void handleIndicies(Global *g, Mesh *mesh) {
    if (g->renderType == VertexArrayOneMesh || g->renderType == VboOneMesh) {
        calcIndiciesOneMesh(mesh, g->tess);
        if (g->renderType == VboOneMesh) {
            uploadDataVBOOneMesh(g, mesh);
        }
    } else {
        calcIndicies(mesh, g->tess);
        if (g->renderType == Vbo) {
            uploadDataVBO(g, mesh);
        }
    }
}

void handleGrid(Global *g, Mesh *mesh) {
    if (g->isUsingShader) {
        calcFlatGrid(*g, mesh);
    } else {
        calcGrid(*g, mesh);
    }
    if (g->renderType == Vbo || g->renderType == VboOneMesh) {
        uploadMeshVBO(g, mesh);
        glBindBuffer(GL_ARRAY_BUFFER, *(g->VboMesh));
        if (g->renderType == VboOneMesh) {
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *(g->VboOneMesh));
        }
    }
}

//Called when tesselation or dimension is changed or
//normal needs to be calculated
void populateMesh(Global *g, Mesh *mesh) {
    allocateSpace(g, mesh);
    handleIndicies(g, mesh);
    handleGrid(g, mesh);
}

//Called when animation is running, just updates the grid's positions
void updateMesh(Global *g, Mesh *mesh) {
    if (g->animate) {
        handleGrid(g, mesh);
    }
}

void uploadMeshVBO(Global *g, Mesh *mesh) {
    glBindBuffer(GL_ARRAY_BUFFER, *(g->VboMesh));
    glBufferData(GL_ARRAY_BUFFER, (g->tess+1) * (g->tess+1) * sizeof(Vertex),
                                        &(mesh->verticies->pos), GL_STATIC_DRAW);
}

void uploadDataVBO(Global *g, Mesh *mesh) {
    int i;
    
    for (i = 0; i < g->nVboIds; i++) {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, g->VboIds[i]);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER,   2 * (g->tess + 1) * sizeof(int), 
                                        mesh->indicies + (i * 2 * (g->tess + 1)), GL_STATIC_DRAW);
        
    }
}

void uploadDataVBOOneMesh(Global *g, Mesh *mesh) {
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *(g->VboOneMesh));
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh->nIndicies * sizeof(int), 
                                        mesh->indicies, GL_STATIC_DRAW);
      
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}


void calcIndicies(Mesh *mesh, int tess) {
    int nRows = tess + 1;
    int nCols = tess + 1;
    int arrPos = 0;
    int meshPos = 0;
    int i, j;

    for (i = 0; i < tess; i++) {
        for (j = 0; j <= tess; j++) {
            mesh->indicies[arrPos++] = meshPos;
            mesh->indicies[arrPos++] = meshPos + nCols;
            meshPos++;
        }
    }
}

void calcIndiciesOneMesh(Mesh *mesh, int tess) {
    int nRows = tess + 1;
    int nCols = tess + 1;
    int arrPos = 0;
    int meshPos = 0;
    int i, j;

    for (i = 0; i < tess/2; i++) {
        for (j = 0; j <= tess; j++) {
            mesh->indicies[arrPos++] = meshPos;
            mesh->indicies[arrPos++] = meshPos + nCols;
            meshPos++;
        }
        meshPos += tess;
        for (j = 0; j <= tess; j++) {
            mesh->indicies[arrPos++] = meshPos;
            mesh->indicies[arrPos++] = meshPos + nCols;
            meshPos--;
        }
        meshPos += tess+2;
    }
}

void calcGrid(Global g, Mesh *mesh) {
    const float A1 = 0.25, k1 = 2.0 * M_PI, w1 = 0.25;
    const float A2 = 0.25, k2 = 2.0 * M_PI, w2 = 0.25;
    int tess = g.tess;
    float stepSize = 2.0 / tess;
    vec3f r, n;
    int i, j;
    float t = g.t;
    int vertexCount = 0;

    for (j = 0; j <= tess; j++) {
        // Sine wave
        for (i = 0; i <= tess; i++) {
            r.x = -1.0 + i * stepSize;
            r.z = -1.0 + j * stepSize;

            if (g.waveDim == 2) {
                r.y = A1 * sinf(k1 * r.x + w1 * t);
                if (g.lighting) {
                    n.x = - A1 * k1 * cosf(k1 * r.x + w1 * t);
                    n.y = 1.0;
                    n.z = 0.0;
                }
            } else if (g.waveDim == 3) {
                r.y = A1 * sinf(k1 * r.x + w1 * t) + A2 * sinf(k2 * r.z + w2 * t);
                if (g.lighting) {
                    n.x = - A1 * k1 * cosf(k1 * r.x + w1 * t);
                    n.y = 1.0;
                    n.z = - A2 * k2 * cosf(k2 * r.z + w2 * t);
                }
            }

            if (g.lighting) {
                mesh->verticies[vertexCount].nor = n;
            }
            mesh->verticies[vertexCount].pos = r;
            vertexCount++;
        }
    }
}

void calcFlatGrid(Global g, Mesh *mesh) {
    int tess = g.tess;
    float stepSize = 2.0 / tess;
    vec3f r, n;
    int i, j;
    int vertexCount = 0;

    n.x = 0;
    n.y = 0;
    n.z = 0;

    for (j = 0; j <= tess; j++) {
        for (i = 0; i <= tess; i++) {
            r.x = -1.0 + i * stepSize;
            r.z = -1.0 + j * stepSize;
            r.y = 0;
            
            mesh->verticies[vertexCount].nor = n;
            mesh->verticies[vertexCount].pos = r;
            vertexCount++;
        }
    }
}
