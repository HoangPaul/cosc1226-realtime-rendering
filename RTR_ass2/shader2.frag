uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;
uniform mat3 normalMatrix;

uniform vec4 lightPosition;
uniform vec4 ambientIntensity;
uniform vec4 ambientMaterial;

uniform vec4 diffuseIntensity;
uniform vec4 diffuseMaterial;

uniform vec4 specularIntensity;
uniform vec4 specularMaterial;
uniform float shininess;

uniform bool isBPhong;
uniform bool isDirectionalLight;

varying vec4 colour;
varying vec3 normal;
varying vec4 vertex;

void main (void) {
    vec3 ecPosition = vec3(modelViewMatrix * vertex);
    vec3 normVector = normalize(normalMatrix * normal);
    vec3 lightVector;
    if (isDirectionalLight) {
        lightVector = normalize(lightPosition.xyz);
    } else {
        vec3 _lightPosition = lightPosition.xyz / lightPosition.w;
        lightVector = normalize(_lightPosition - ecPosition);
    }
    vec3 reflectVector = reflect(-lightVector, normVector); 
    vec3 viewVec = normalize(vec3(0,0,1));
    vec3 halfVec = normalize(lightVector + viewVec);    

    float NdotL = dot(normVector, lightVector);
    float NdotH = dot(normVector, halfVec); 
    float RdotV = dot(reflectVector, viewVec);

    vec4 lambert = diffuseIntensity * diffuseMaterial * max(NdotL, 0.0);
    vec4 ambient = ambientIntensity * ambientMaterial;    
    vec4 bphong = specularIntensity * specularMaterial * pow(max(NdotH, 0), shininess);
    vec4 phong = specularIntensity * specularMaterial * pow(max(RdotV, 0), shininess);

    if (isBPhong) {
        gl_FragColor = lambert + ambient + bphong;
    } else {
        gl_FragColor = lambert + ambient + phong;
    }
}
