#ifndef H_MESH
#define H_MESH

#include "main.h"

typedef struct vec3f { float x, y, z; } vec3f;
typedef struct Vertex Vertex;
typedef struct Mesh Mesh;

struct Vertex {
    vec3f pos;
    vec3f nor;
};

struct Mesh {
    int tess;        
    int nVerts;
    Vertex *verticies;
    int nIndicies;
    int *indicies;
};

void populateMesh(Global *g, Mesh *mesh);
void updateMesh(Global *g, Mesh *mesh);
void initVboData(Global *g);

#endif
