uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;
uniform mat3 normalMatrix;

uniform vec4 lightPosition;
uniform vec4 ambientIntensity;
uniform vec4 ambientMaterial;

uniform vec4 diffuseIntensity;
uniform vec4 diffuseMaterial;

uniform vec4 specularIntensity;
uniform vec4 specularMaterial;
uniform float shininess;

uniform float time;
uniform bool is3D;
uniform bool isBPhong;
uniform bool isDirectionalLight;

varying vec4 colour;

void main(void)
{
    float A1 = 0.25, k1 = 2.0 * 3.1415, w1 = 0.25;

    vec4 newVert = gl_Vertex;    
    vec3 newNorm = gl_Normal;

    newVert.y = A1 * sin(k1 * gl_Vertex.x + w1 * time);
    if (is3D) {
       newVert.y += A1 * sin(k1 * gl_Vertex.z + w1 * time); 
    }

    newNorm.x = -A1 * k1 * cos(k1 * gl_Vertex.x + w1 * time);
    newNorm.y = 1.0;
    newNorm.z = 0.0;
    if (is3D) {
        newNorm.z = -A1 * k1 * cos(k1 * gl_Vertex.z + w1 * time);
    }

    newNorm = normalize(newNorm);

    vec3 ecPosition = vec3(modelViewMatrix * newVert);
    vec3 normVector = normalize(normalMatrix * newNorm);
    vec3 lightVector;
    if (isDirectionalLight) {
        lightVector = normalize(lightPosition.xyz);
    } else {
        vec3 _lightPosition = lightPosition.xyz / lightPosition.w;
        lightVector = normalize(_lightPosition - ecPosition);
    }
    vec3 reflectVector = reflect(-lightVector, normVector);
    vec3 viewVec = normalize(vec3(0,0,1));
    vec3 halfVec = normalize(lightVector + viewVec);

    float NdotL = dot(normVector, lightVector);
    float NdotH = dot(normVector, halfVec); 
    float RdotV = dot(reflectVector, viewVec);

    vec4 lambert = diffuseIntensity * diffuseMaterial * max(NdotL, 0.0);
    vec4 ambient = ambientIntensity * ambientMaterial;    
    vec4 bphong = specularIntensity * specularMaterial * pow(max(NdotH, 0), shininess);
    vec4 phong = specularIntensity * specularMaterial * pow(max(RdotV, 0), shininess);

    if (isBPhong) {
        colour = lambert + ambient + bphong;
    } else {
        colour = lambert + ambient + phong;
    }

    gl_Position = projectionMatrix * modelViewMatrix * newVert;
}
