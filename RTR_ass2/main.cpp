#include "main.h"
#include "draw.h"
#include "mesh.h"
#include "shaders.h"

typedef void (*displayFP)(Camera *c, Mesh *m);

Global g = {    false,  //animate
    0.0,    //t
    0.0,    //lastT
    line,   //polygonMode
    true,  //lighting
    false,  //drawNormals
    0,      //width
    0,      //height
    8,      //tess
    2,      //waveDim                                                       
    0,      //frameCount
    0.0,    //frameRate
    1.0,    //displayStatsInterval
    0,      //lastStatsDisplayT
    OldDraw,      //RenderType
    {1,0,0,0,0,0,0,0,0},//enabledLight
    0,      //nVboIds
    0,      //VboIds
    0,      //VboMesh
    0,      //VboOneMesh
    0,      //sleep
    0.0,    //sleepTime
    0,      //consolePM
    1,      //displayOSD
    true
};

bool debug[d_nflags] = { false, false, false, false, false, false }; 
const float milli = 1000.0;

displayFP displayFunc;
static SDL_Window *mainWindow = 0;

bool wantCalculation = false;
bool wantUpdate = false;
bool wantRedisplay = false;

unsigned int updateCount = 0;
bool isFlat;
glm::vec4 defaultLightPosition = glm::vec4(1, 1, 1, 0); //This is direction if using directional light
float lX = defaultLightPosition.x;
float lY = defaultLightPosition.y;
float lZ = defaultLightPosition.z;

/* --- Uniforms --- */
glm::mat4 modelViewMatrix;
glm::mat4 projectionMatrix;
glm::mat3 normalMatrix;
GLuint perVertexProg;
GLuint perPixelProg;
GLuint currProg;
GLuint savedProg;
bool is3D;
bool isBPhong;
bool isDirectionalLight;
float shininess = 20;
float attenuationFactor = 1;

glm::vec4 ambientIntensity = glm::vec4(0.0, 0.0, 0.0, 1.0);
glm::vec4 ambientMaterial = glm::vec4(1.0);
glm::vec4 diffuseIntensity = glm::vec4(1.0);
glm::vec4 diffuseMaterial = glm::vec4(0.0, 1.0, 1.0, 1.0);
glm::vec4 specularIntensity = glm::vec4(1.0);
glm::vec4 specularMaterial = glm::vec4(1.0, 1.0, 1.0, 1.0);
glm::vec4 lightPosition = glm::vec4(lX, lY, lZ, 1);

void translateLight();
void initLights();
void updateUniforms();

void init(Mesh *mesh) {    
    glClearColor(0.0, 0.0, 0.0, 1.0);
    glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
    glEnable(GL_DEPTH_TEST);

    g.tess = 8;
    mesh->tess = 0;           
    mesh->nVerts = 0;
    mesh->nIndicies = 0;
    g.renderType = VertexArray;
    g.sleep = 0;
    is3D = false;
    isBPhong = false;
    isDirectionalLight = true;
    isFlat = false;

    modelViewMatrix = glm::mat4(1.0);

    perVertexProg = getShader("shader.vert", "shader.frag");
    perPixelProg = getShader("shader2.vert", "shader2.frag");
    savedProg = 0;
    currProg = perPixelProg;
    glUseProgram(currProg);    
    updateUniforms();

    initVboData(&g);
    populateMesh(&g, mesh);
    initDefaultLights();
}

void updateUniforms() {
    if (g.isUsingShader) {
        glUniform4fv(glGetUniformLocation(currProg, "lightPosition"), 1, &lightPosition[0]);
        glUniform1f(glGetUniformLocation(currProg, "time"), 0);
        glUniform1i(glGetUniformLocation(currProg, "isBPhong"), isBPhong);
        glUniform1i(glGetUniformLocation(currProg, "is3D"), is3D);
        glUniform1i(glGetUniformLocation(currProg, "isDirectionalLight"), isDirectionalLight);
        glUniform1f(glGetUniformLocation(currProg, "shininess"), shininess);
        glUniform4fv(glGetUniformLocation(currProg, "ambientIntensity"), 1, &ambientIntensity[0]);
        glUniform4fv(glGetUniformLocation(currProg, "ambientMaterial"), 1, &ambientMaterial[0]);
        glUniform4fv(glGetUniformLocation(currProg, "diffuseIntensity"), 1, &diffuseIntensity[0]);
        glUniform4fv(glGetUniformLocation(currProg, "diffuseMaterial"), 1, &diffuseMaterial[0]);
        glUniform4fv(glGetUniformLocation(currProg, "specularIntensity"), 1, &specularIntensity[0]);
        glUniform4fv(glGetUniformLocation(currProg, "specularMaterial"), 1, &specularMaterial[0]);
        glUniformMatrix4fv(glGetUniformLocation(currProg, "modelViewMatrix"), 1, false, &modelViewMatrix[0][0]);
        normalMatrix = glm::transpose(glm::inverse(glm::mat3(modelViewMatrix)));
        glUniformMatrix3fv(glGetUniformLocation(currProg, "normalMatrix"), 1, false, &normalMatrix[0][0]);
        glUniformMatrix4fv(glGetUniformLocation(currProg, "projectionMatrix"), 1, false, &projectionMatrix[0][0]);

    }
}

void transformLights() {
    if (isDirectionalLight) {
        lightPosition = glm::vec4(lX, lY, lZ, 0);
        lightPosition.w = 0;
        lightPosition = modelViewMatrix * lightPosition;
    } else {
        lightPosition = glm::vec4(lX, lY, lZ, 1);
        lightPosition = modelViewMatrix * lightPosition;
    }
    if (g.isUsingShader)
        glUniform4fv(glGetUniformLocation(currProg, "lightPosition"), 1, &lightPosition[0]);
}

void displayOSD() {
    char buffer[30];
    char *bufp;
    int w, h;


    glPushAttrib(GL_ENABLE_BIT | GL_CURRENT_BIT);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_LIGHTING);

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();

    //Set up orthographic coordinate system to match the window, 
    //i.e. (0,0)-(w,h) 
    SDL_GetWindowSize(mainWindow, &w, &h);
    glOrtho(0.0, w, 0.0, h, -1.0, 1.0);

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    glUseProgram(0);

    // Render mode
    glColor3f(1.0, 1.0, 0.0);
    glRasterPos2i(10, 80);
    switch (g.renderType) {
        //The empty spaces exists to ensure every render type has the same number of chars to render.
        //Yes, it does has an effect on framerate for CPU limited bottlenecks 
        //less chars -> high framerate
    case ImmediateMode: 
        strcpy(buffer, "Immediate mode");
        break;
    case VertexArray: 
        strcpy(buffer, "VA            ");
        break;
    case OldDraw: 
        strcpy(buffer, "Old draw      ");
        break;
    case VertexArrayOneMesh: 
        strcpy(buffer, "VA - one mesh ");
        break;
    case Vbo:
        strcpy(buffer, "VBO           ");
        break;
    case VboOneMesh:
        strcpy(buffer, "VBO - one mesh");
        break;
    }
    for (bufp = buffer; *bufp; bufp++) {
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *bufp);           
    }

    // Frame rate 
    glColor3f(1.0, 1.0, 0.0);
    glRasterPos2i(10, 60);
    //snprintf(buffer, sizeof buffer, "frame rate (f/s):  %5.0f", g.frameRate);
    sprintf(buffer, "frame rate (f/s):  %5.0f", g.frameRate);
    for (bufp = buffer; *bufp; bufp++) {
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *bufp);            
    }

    // Frame time 
    glColor3f(1.0, 1.0, 0.0);
    glRasterPos2i(10, 40);
    //snprintf(buffer, sizeof buffer, "frame time (ms/f): %5.0f", 1.0 / g.frameRate * milli);
    sprintf(buffer, "frame time (ms/f): %5.0f", 1.0 / g.frameRate * milli);
    for (bufp = buffer; *bufp; bufp++) {
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15,  *bufp);
    }
    // Tesselation 
    glColor3f(1.0, 1.0, 0.0);
    glRasterPos2i(10, 20);
    //snprintf(buffer, sizeof buffer, "tesselation:       %5d", g.tess);
    sprintf(buffer, "tesselation:       %5d", g.tess);
    for (bufp = buffer; *bufp; bufp++) {
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *bufp);
    }

    glUseProgram(currProg);
    glPopMatrix();  // Pop modelview 
    glMatrixMode(GL_PROJECTION);

    glPopMatrix();  // Pop projection 
    glMatrixMode(GL_MODELVIEW);

    glPopAttrib();
}

void consolePM() {
    printf("frame rate (f/s):  %5.0f\n", g.frameRate);
    printf("frame time (ms/f): %5.0f\n", 1.0 / g.frameRate * 1000.0);
    printf("tesselation:       %5d\n", g.tess);
}


void displayWithShaders(Camera *camera, Mesh *mesh) {
    modelViewMatrix = glm::mat4(1.0);
    normalMatrix = glm::mat3(1.0);
    
    modelViewMatrix = glm::rotate(modelViewMatrix, camera->rotateX * glm::pi<float>() / 180.0f, glm::vec3(1.0, 0.0, 0.0));
    modelViewMatrix = glm::rotate(modelViewMatrix, camera->rotateY * glm::pi<float>() / 180.0f, glm::vec3(0.0, 1.0, 0.0));
    modelViewMatrix = glm::scale(modelViewMatrix, glm::vec3(camera->scale));
    glUniformMatrix4fv(glGetUniformLocation(currProg, "modelViewMatrix"), 1, false, &modelViewMatrix[0][0]);
    normalMatrix = glm::transpose(glm::inverse(glm::mat3(modelViewMatrix)));
    glUniformMatrix3fv(glGetUniformLocation(currProg, "normalMatrix"), 1, false, &normalMatrix[0][0]);

    transformLights();

    glUseProgram(0);
    glDisable(GL_LIGHTING);
    drawAxes(modelViewMatrix, 5.0);
    if (isDirectionalLight)
        drawDirection(modelViewMatrix, lX, lY, lZ, 5.0);
    glEnable(GL_LIGHTING);
    glUseProgram(currProg); 

    drawSineWave(&g, mesh, g.tess);
}

void displayWithoutShaders(Camera *camera, Mesh *mesh) {
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glPushMatrix();
    glRotatef(camera->rotateX, 1.0, 0.0, 0.0);
    glRotatef(camera->rotateY, 0.0, 1.0, 0.0);
    glScalef(camera->scale, camera->scale, camera->scale);

    drawAxes(glm::mat4(1), 5.0);

    glEnable(GL_LIGHTING);
    glEnable(GL_NORMALIZE);
    glEnable(GL_LIGHT0);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    
    drawSineWave(&g, mesh, g.tess);

    glDisable(GL_LIGHTING);

    glPopMatrix();
}

void display(Camera *camera, Mesh *mesh) {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glViewport(0, 0, g.width, g.height);

    if (g.isUsingShader) {
        displayWithShaders(camera, mesh);
    } else {
        displayWithoutShaders(camera, mesh);
    }

    if (g.displayOSD) {
        displayOSD();
    }

    SDL_GL_SwapWindow(mainWindow);

    g.frameCount++;
}



void displayMultiView(Camera *camera, Mesh *mesh)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glm::mat4 modelViewMatrixSave(modelViewMatrix);
    glm::mat3 normalMatrixSave(normalMatrix);

    // Front view
    modelViewMatrix = glm::mat4(1.0);
    glViewport(g.width / 16.0, g.height * 9.0 / 16.0, g.width * 6.0 / 16.0, g.height * 6.0 / 16.0);
    if (g.isUsingShader)
        glUniformMatrix4fv(glGetUniformLocation(currProg, "modelViewMatrix"), 1, false, &modelViewMatrix[0][0]);
    glUseProgram(0);
    drawAxes(modelViewMatrix, 5.0);
    glUseProgram(currProg); 
    drawSineWave(&g, mesh, g.tess);

    // Top view
    modelViewMatrix = glm::mat4(1.0);
    modelViewMatrix = glm::rotate(modelViewMatrix, glm::pi<float>() / 2.0f, glm::vec3(1.0, 0.0, 0.0));
    glViewport(g.width / 16.0, g.height / 16.0, g.width * 6.0 / 16.0, g.height * 6.0 / 16);
    if (g.isUsingShader)
        glUniformMatrix4fv(glGetUniformLocation(currProg, "modelViewMatrix"), 1, false, &modelViewMatrix[0][0]);
    glUseProgram(0);
    drawAxes(modelViewMatrix, 5.0);
    glUseProgram(currProg); 
    drawSineWave(&g, mesh, g.tess);

    // Left view
    modelViewMatrix = glm::mat4(1.0);
    modelViewMatrix = glm::rotate(modelViewMatrix, glm::pi<float>() / 2.0f, glm::vec3(0.0, 1.0, 0.0));
    glViewport(g.width * 9.0 / 16.0, g.height * 9.0 / 16.0, g.width * 6.0 / 16.0, g.height * 6.0 / 16.0);
    if (g.isUsingShader)
        glUniformMatrix4fv(glGetUniformLocation(currProg, "modelViewMatrix"), 1, false, &modelViewMatrix[0][0]);
    glUseProgram(0);
    drawAxes(modelViewMatrix, 5.0);
    glUseProgram(currProg); 
    drawSineWave(&g, mesh, g.tess);

    // General view
    modelViewMatrix = glm::mat4(1.0);
    modelViewMatrix = glm::scale(modelViewMatrix, glm::vec3(camera->scale));
    modelViewMatrix = glm::rotate(modelViewMatrix, camera->rotateY * glm::pi<float>() / 180.0f, glm::vec3(0.0, 1.0, 0.0));
    modelViewMatrix = glm::rotate(modelViewMatrix, camera->rotateX * glm::pi<float>() / 180.0f, glm::vec3(1.0, 0.0, 0.0));
    normalMatrix = glm::transpose(glm::inverse(glm::mat3(modelViewMatrix)));
    glViewport(g.width * 9.0 / 16.0, g.width / 16.0, g.width * 6.0 / 16.0, g.height * 6.0 / 16.0);
    if (g.isUsingShader) {
        glUniformMatrix4fv(glGetUniformLocation(currProg, "modelViewMatrix"), 1, false, &modelViewMatrix[0][0]);
        glUniformMatrix3fv(glGetUniformLocation(currProg, "normalMatrix"), 1, false, &normalMatrix[0][0]);
    }
    glUseProgram(0);
    drawAxes(modelViewMatrix, 5.0);
    glUseProgram(currProg); 
    drawSineWave(&g, mesh, g.tess);

    SDL_GL_SwapWindow(mainWindow);

    g.frameCount++;

}

void reshape(int w, int h) {
    g.width = w;
    g.height = h;
    glViewport(0, 0, (GLsizei) w, (GLsizei) h); 
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-1.0, 1.0, -1.0, 1.0, -100.0, 100.0);
    projectionMatrix = glm::ortho(-1.0, 1.0, -1.0, 1.0, -100.0, 100.0);
    if (currProg != 0)
        glUniformMatrix4fv(glGetUniformLocation(currProg, "projectionMatrix"), 1, false, &projectionMatrix[0][0]);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

//recalculate the whole mesh data
void recalculate(Mesh *mesh) {
    populateMesh(&g, mesh);
}

//update for animation
void update(Mesh *mesh) {
    updateMesh(&g, mesh);
}

/* dt in ms */
/*
void sleep(float dt) {
struct timespec req;
struct timespec rem;

if (dt <= 0) {
return;
}

req.tv_sec = 0;
req.tv_nsec = dt * 1000000;
rem.tv_sec = 0;
rem.tv_nsec = 0;

nanosleep(&req, &rem);
}
*/ 
void idle() {
    float t, dt;

    t = SDL_GetTicks() / milli;

    // Accumulate time if animation enabled
    if (g.animate) {
        dt = t - g.lastT; 
        g.t += dt;
        if (g.isUsingShader)
            glUniform1f(glGetUniformLocation(currProg, "time"), g.t);
        g.lastT = t;
        if (debug[d_animation])
            printf("idle: animate %f\n", g.t);
        wantUpdate = true;
    }

    // Update stats, although could make conditional on a flag set interactively
    dt = (t - g.lastStatsDisplayT);
    if (dt > g.displayStatsInterval) {
        g.frameRate = g.frameCount / dt;
        if (debug[d_OSD])
            printf("dt %f framecount %d framerate %f\n", dt, g.frameCount, g.frameRate);
        g.lastStatsDisplayT = t;
        g.frameCount = 0;
        if (g.consolePM) {
            consolePM();
        }
    }
    /*
    float sleepT;
    if (g.sleep) {
    sleepT = ((1.0/30.0) * milli) - ((t - g.sleepTime) * milli);
    if (sleepT < 0) {
    sleepT = 0;
    }
    sleep(sleepT);
    }
    g.sleepTime = SDL_GetTicks() / milli;
    */
    wantRedisplay = 1;
}

void keyboard(SDL_Keycode key) {
    GLuint temp;
    switch (key) {
    case SDLK_ESCAPE:
        printf("exit\n");
        exit(0);
        break;
    case SDLK_a:
        g.animate = !g.animate;
        if (g.animate) {
            g.lastT = SDL_GetTicks() / milli;
        } 
        printf("[a] Is animating: %d\n", g.animate);
        break;
    case SDLK_f:
        isFlat = !isFlat;
        if (isFlat) {
            glShadeModel(GL_FLAT);
            printf("[f] Changed to flat shading\n");
        } else {
            glShadeModel(GL_SMOOTH);
            printf("[f] Changed to smooth shading\n");
        }
        break;
    case SDLK_l:
        g.lighting = !g.lighting;
        printf("[l] Is Fixed pipeline lighting: %d\n", g.lighting);
        wantRedisplay = true;
        wantCalculation = true;
        break;
    case SDLK_m:
        isBPhong = !isBPhong;
        printf("[m] Is currently BPhong: %d\n", isBPhong);
        glUniform1i(glGetUniformLocation(currProg, "isBPhong"), isBPhong);
        break;
    case SDLK_w:
        if (g.polygonMode == line) {
            g.polygonMode = fill;
            printf("[w] Changed to filled polygon mode\n");
        } else {
            g.polygonMode = line;
            printf("[w] Changed to line polygon mode\n");
        }
        wantRedisplay = true;
        break;

    case SDLK_n:
        g.drawNormals = !g.drawNormals;
        printf("[n] Is drawing normals: %d\n", g.drawNormals);
        wantRedisplay = true;
        break;
    case SDLK_p:
        glUseProgram(0);
        if (currProg == perPixelProg) {
            currProg = perVertexProg;
            printf("[p] Changed to per vertex shading\n");
        } else if (currProg == perVertexProg) {
            currProg = perPixelProg;
            printf("[p] Changed to per fragment shading\n");
        }
        glUseProgram(currProg);  
        updateUniforms();
        wantRedisplay = true;
        wantUpdate = true;
        break;
    case SDLK_s:
        temp = savedProg;
        glUseProgram(savedProg);
        savedProg = currProg;
        currProg = temp;
        g.isUsingShader = !g.isUsingShader;
        updateUniforms();
        wantRedisplay = true;
        wantCalculation = true;
        printf("[s] Is using shaders %d\n", g.isUsingShader);
        break;
    case SDLK_x: 
        displayFunc = &displayMultiView;
        printf("[x] Changed view to multiview> Press Z to change to single view \n");
        wantRedisplay = true;
    case SDLK_z: 
        displayFunc = &display;
        printf("[z] Changed view to single view. Press x to change to mutli view \n");
        wantRedisplay = true;
        break;
    case SDLK_KP_PLUS:
        g.tess *= 2;
        wantRedisplay = true;
        wantCalculation = true;
        printf("[+] Increased tesselation to: %d \n", g.tess);
        break;
    case SDLK_KP_MINUS:
        g.tess /= 2;
        if (g.tess < 8)
            g.tess = 8;
        wantRedisplay = true;
        wantCalculation = true;
        printf("[-] Decreased tesselation to: %d \n", g.tess);
        break;
    case SDLK_d:
        g.waveDim++;
        if (g.waveDim > 3)
            g.waveDim = 2;
        is3D = (g.waveDim == 3);
        if (g.isUsingShader)
            glUniform1i(glGetUniformLocation(currProg, "is3D"), is3D);
        wantCalculation = true;
        printf("[d] Is 3D: %d \n", is3D);
        break;
    case SDLK_i:
        g.renderType = (RenderType)((int)g.renderType+1);
        if (g.renderType == VertexArrayOneMesh) {
            g.renderType = (RenderType)((int)g.renderType+1);
        }
        if (g.renderType == VboOneMesh) {
            g.renderType = (RenderType)((int)g.renderType+1); //Skip VboOneMesh
        }
        if (g.renderType > 5) {
            g.renderType = (RenderType)1; //Skip old draw
        }
        wantRedisplay = true;
        wantCalculation = true;
        printf("[i] Changed render type\n");
        break;
        /*
        case SDLK_f:
        g.sleep = !g.sleep;
        wantRedisplay = true;
        break;
        */
    case SDLK_c:
        isDirectionalLight = !isDirectionalLight;
        printf("Is using directional light: %i\n", isDirectionalLight);
        lightPosition.x = defaultLightPosition.x;
        lightPosition.y = defaultLightPosition.y;
        lightPosition.z = defaultLightPosition.z;
        lightPosition.w = isDirectionalLight ? 0 : 1;
        if (g.isUsingShader)
            glUniform1i(glGetUniformLocation(currProg, "isDirectionalLight"), isDirectionalLight);
        printf("[c] Is using directional light: %d \n", isDirectionalLight);
        break;
    case SDLK_o:
        g.displayOSD = !g.displayOSD;
        g.consolePM = !g.consolePM;
        wantRedisplay = true;
        printf("[o] Toggled console and screen perf meter display\n");

        break;
    case SDLK_g:
        shininess++;
        if (g.isUsingShader)
            glUniform1f(glGetUniformLocation(currProg, "shininess"), shininess);
        glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, &shininess);
        printf("[g] Increased shininess to %f\n", shininess);
        break;
    case SDLK_h:
        shininess--;
        if (shininess < 0) {
            shininess = 0;
        }
        if (g.isUsingShader)
            glUniform1f(glGetUniformLocation(currProg, "shininess"), shininess);
        glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, &shininess);
        printf("[h] Decreased shininess to %f\n", shininess);
        break;
    case SDLK_1:
        lX += 0.1;
        break;
    case SDLK_2:
        lY += 0.1;
        break;
    case SDLK_3:
        lZ += 0.1;
        break;
    case SDLK_4:
        lX -= 0.1;
        break;
    case SDLK_5:
        lY -= 0.1;
        break;
    case SDLK_6:
        lZ -= 0.1;
        break;
    default:
        break;
    }
}

void motion(Camera *camera, int x, int y) {
    float dx, dy;

    if (debug[d_mouse]) {
        printf("motion: %d %d\n", x, y);
        printf("camera->rotate: %f %f\n", camera->rotateX, camera->rotateY);
        printf("camera->scale:%f\n", camera->scale);
    }

    dx = x - camera->lastX;
    dy = y - camera->lastY;
    camera->lastX = x;
    camera->lastY = y;

    switch (camera->control) {
    case inactive:
        break;
    case rotate:
        camera->rotateX += dy;
        camera->rotateY += dx;
        break;
    case pan:
        break;
    case zoom:
        camera->scale += dy / 100.0;
        break;
    }

    wantRedisplay = true;
}

void mouse(Camera *camera, unsigned int button, unsigned int state, int x, int y) {
    if (debug[d_mouse])
        printf("mouse: %d %d %d\n", button, x, y);

    camera->lastX = x;
    camera->lastY = y;

    if (state == SDL_MOUSEBUTTONDOWN)
        switch(button) {
        case SDL_BUTTON_LEFT:
            camera->control = rotate;
            break;
        case SDL_BUTTON_MIDDLE:
            camera->control = pan;
            break;
        case SDL_BUTTON_RIGHT:
            camera->control = zoom;
            break;
    }
    else if (state == SDL_MOUSEBUTTONUP)
        camera->control = inactive;
}

bool handleEvents(Camera *camera) {
    SDL_Event event;

    while (SDL_PollEvent(&event)) {
        switch (event.type) {
        case SDL_MOUSEMOTION:
            motion(camera, event.motion.x, event.motion.y);
            break;
        case SDL_MOUSEBUTTONDOWN:
            mouse(camera, event.button.button, SDL_MOUSEBUTTONDOWN, event.button.x, event.button.y);
            break;
        case SDL_MOUSEBUTTONUP:
            mouse(camera, event.button.button, SDL_MOUSEBUTTONUP, event.button.x, event.button.y);
            break;
        case SDL_KEYDOWN:
            keyboard(event.key.keysym.sym);
            break;
        case SDL_WINDOWEVENT:
            switch (event.window.event) {
            case SDL_WINDOWEVENT_RESIZED:
                if (event.window.windowID == SDL_GetWindowID(mainWindow)) {
                    SDL_SetWindowSize(mainWindow, event.window.data1, event.window.data2);
                    reshape(event.window.data1, event.window.data2);
                }
                break;
            case SDL_WINDOWEVENT_CLOSE:
                return true;
                break;
            default:
                break;  
            }
            break;

        default:
            break;
        }
    }

    return false;
}

void mainloop(Camera *camera, Mesh *mesh) {
    while (1) {
        if (handleEvents(camera)) {
            exit(-1);
        }
        idle();
        if (wantCalculation) {
            recalculate(mesh);
            wantCalculation = 0;
        } else if (wantUpdate) {
            update(mesh);
            wantUpdate = 0;
        }
        if (wantRedisplay) {
            displayFunc(camera, mesh);
            wantRedisplay = 0;
        }
    }
}

void initGraphics() {
    SDL_GLContext mainGLContext;

    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);

    mainWindow = SDL_CreateWindow("Assignment 1", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 1920, 1080, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);

    if (mainWindow == NULL) {
        fprintf(stderr, "Failed to create window: %s\n", SDL_GetError());
        exit(EXIT_SUCCESS);
    }

    mainGLContext = SDL_GL_CreateContext(mainWindow);
    SDL_GL_MakeCurrent(mainWindow, mainGLContext);
}

int main(int argc, char** argv) {
    Camera camera = { 0, 0, 30.0, -30.0, 1.0, inactive };
    Mesh mesh;

    glutInit(&argc, argv);

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        fprintf(stderr, "Unable to init SDL: %s\n", SDL_GetError());
        return EXIT_SUCCESS;
    }

    initGraphics();

#if _WIN32
    GLenum err = glewInit() ; 

    if (GLEW_OK != err) { 
        printf("%s\n", glewGetErrorString(err));
        char c;
        scanf("%d", &c);
        exit(1);
    }
#endif

    init(&mesh);
    displayFunc = &display;

    mainloop(&camera, &mesh);
    return 0;
}
