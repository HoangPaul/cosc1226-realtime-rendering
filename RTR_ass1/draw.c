#include "main.h"
#include "mesh.h"
#include "draw.h"

//I don't think this is the values for cyan, but I'll leave it anyway
color3f cyan = { 1.0, 0.0, 1.0 };

GLfloat light_position[9][4] = {    {  1.0,  1.0,  1.0, 0.0},
                                    { -1.0,  1.0,  1.0, 0.0},
                                    { -1.0,  1.0, -1.0, 0.0},
                                    {  1.0,  1.0, -1.0, 0.0},
                                    {  1.0, -1.0,  1.0, 0.0},
                                    { -1.0, -1.0,  1.0, 0.0},
                                    { -1.0, -1.0, -1.0, 0.0},
                                    {  0.0,  0.0,  1.0, 0.0},
                                    {  1.0, -1.0, -1.0, 0.0} };

GLenum lights[8] = {    GL_LIGHT0, 
                        GL_LIGHT1,
                        GL_LIGHT2,
                        GL_LIGHT3,
                        GL_LIGHT4,
                        GL_LIGHT5,
                        GL_LIGHT6,
                        GL_LIGHT7};

void initLights() {
    GLfloat light_ambient[] = { 0.0, 0.0, 0.0, 0.2 };
    GLfloat light_specular[] = { 1.0, 1.0, 1.0, 0.2 };
    GLfloat light_dir[] = { 0,0,0 };
    int i;

    for (i = 1; i < 8; i++) {
        GLfloat light_diffuse[] = { (rand()%50)/100.0, 
                                    (rand()%50)/100.0,
                                    (rand()%50)/100.0, 0.2 };
        glLightfv(lights[i], GL_AMBIENT, light_ambient);
        glLightfv(lights[i], GL_DIFFUSE, light_diffuse);
        glLightfv(lights[i], GL_SPECULAR, light_specular);   
        glLightfv(lights[i], GL_POSITION, light_position[i]);
        glLightfv(lights[i], GL_SPOT_DIRECTION, light_dir);
    }
}

//Drawing function as provided in source code
void oldDraw(Global *g, int tess) {
    const float A1 = 0.25, k1 = 2.0 * M_PI, w1 = 0.25;
    const float A2 = 0.25, k2 = 2.0 * M_PI, w2 = 0.25;
    float stepSize = 2.0 / tess;
    vec3f r, n;
    int i, j;
    float t = g->t;

    for (j = 0; j < tess; j++) {
        // Sine wave
        glBegin(GL_QUAD_STRIP);
        for (i = 0; i <= tess; i++) {
            r.x = -1.0 + i * stepSize;
            r.z = -1.0 + j * stepSize;

            if (g->waveDim == 2) {
                r.y = A1 * sinf(k1 * r.x + w1 * t);
                if (g->lighting) {
                    n.x = - A1 * k1 * cosf(k1 * r.x + w1 * t);
                    n.y = 1.0;
                    n.z = 0.0;
                }
            } else if (g->waveDim == 3) {
                r.y = A1 * sinf(k1 * r.x + w1 * t) + A2 * sinf(k2 * r.z + w2 * t);
                if (g->lighting) {
                    n.x = - A1 * k1 * cosf(k1 * r.x + w1 * t);
                    n.y = 1.0;
                    n.z = - A2 * k2 * cosf(k2 * r.z + w2 * t);
                }
            }

            if (g->lighting) 
                glNormal3fv((GLfloat *)&n);
            glVertex3fv((GLfloat *)&r);

            r.z += stepSize;

            if (g->waveDim == 3) {
                r.y = A1 * sinf(k1 * r.x + w1 * t) + A2 * sinf(k2 * r.z + w2 * t);
                if (g->lighting) {
                    n.z = - A2 * k2 * cosf(k2 * r.z + w2 * t);
                }
            }

            if (g->lighting) 
                glNormal3fv((GLfloat *)&n);
            glVertex3fv((GLfloat *)&r);
        }
        glEnd();
    }

    if (g->lighting) {
        glDisable(GL_LIGHTING);
    }

    // Normals
    if (g->drawNormals) {
        for (j = 0; j <= tess; j++) {
            for (i = 0; i <= tess; i++) {
                r.x = -1.0 + i * stepSize;
                r.z = -1.0 + j * stepSize;

                n.y = 1.0;
                n.x = - A1 * k1 * cosf(k1 * r.x + w1 * t);
                if (g->waveDim == 2) {
                    r.y = A1 * sinf(k1 * r.x + w1 * t);
                    n.z = 0.0;
                } else {
                    r.y = A1 * sinf(k1 * r.x + w1 * t) + A2 * sinf(k2 * r.z + w2 * t);
                    n.z = - A2 * k2 * cosf(k2 * r.z + w2 * t);
                }

                drawVector(&r, &n, 0.05, true, &cyan);
            }
        }
    }
}

void drawGridVBO(Global *g, Mesh *mesh) {
    int i, j;
    GLenum glErr;

    for (j = 0; j < g->nVboIds; j++) {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, g->VboIds[j]);
        glEnableClientState(GL_VERTEX_ARRAY);
        glVertexPointer(3, GL_FLOAT, sizeof(Vertex), 0);

        if (g->lighting) {
            glEnableClientState(GL_NORMAL_ARRAY);
            glNormalPointer(GL_FLOAT, sizeof(Vertex), (void*)(sizeof(vec3f)));
        }

        glDrawElements(GL_TRIANGLE_STRIP, (mesh->tess+1) * 2, GL_UNSIGNED_INT, 0);
        glDisableClientState(GL_VERTEX_ARRAY);

        if (g->lighting) {
            glDisableClientState(GL_NORMAL_ARRAY);
        }
    }
  
}

void drawGridVBOOneMesh(Global *g, Mesh *mesh) {
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, sizeof(Vertex), 0);

    if (g->lighting) {
        glEnableClientState(GL_NORMAL_ARRAY);
        glNormalPointer(GL_FLOAT, sizeof(Vertex), (void*)(sizeof(vec3f)));
    }

    glDrawElements(GL_TRIANGLE_STRIP, mesh->nIndicies, GL_UNSIGNED_INT, 0);
    glDisableClientState(GL_VERTEX_ARRAY);

    if (g->lighting) {
        glDisableClientState(GL_NORMAL_ARRAY);
    }
}

void drawGridVertexArrays(Global *g, Mesh *mesh) {
    int i;

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, sizeof(Vertex), &mesh->verticies[0].pos);
    if (g->lighting) {
        glEnableClientState(GL_NORMAL_ARRAY);
        glNormalPointer(GL_FLOAT, sizeof(Vertex), &mesh->verticies[0].nor);
    }

    for (i = 0; i < mesh->tess; i++) {
        glDrawElements(GL_TRIANGLE_STRIP, (mesh->tess+1) * 2, GL_UNSIGNED_INT, &mesh->indicies[i * (mesh->tess+1) * 2]); 
    }
    glDisableClientState(GL_VERTEX_ARRAY);
    if (g->lighting) {
        glDisableClientState(GL_NORMAL_ARRAY);
    }

}

void drawGridVertexArraysOneMesh(Global *g, Mesh *mesh) {
    int i;
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, sizeof(Vertex), &mesh->verticies[0].pos);
    if (g->lighting) {
        glEnableClientState(GL_NORMAL_ARRAY);
        glNormalPointer(GL_FLOAT, sizeof(Vertex), &mesh->verticies[0].nor);
    }

    glDrawElements(GL_TRIANGLE_STRIP, mesh->nIndicies, GL_UNSIGNED_INT, mesh->indicies); 
    glDisableClientState(GL_VERTEX_ARRAY);
    if (g->lighting) {
        glDisableClientState(GL_NORMAL_ARRAY);
    }
}

//uses pre-computed values and shows them in immediate mode
void drawGridImmediateMode(Global *g, Mesh *mesh, int tess) {
    int nCols = tess + 1;
    int vertexCounter = 0;
    int i, j;

    for (i = 0; i < tess; i++) {
        glBegin(GL_TRIANGLE_STRIP);
        for (j = 0; j <= tess; j++) {
            //Vertex
            if (g->lighting) {
                glNormal3fv((GLfloat *)&mesh->verticies[vertexCounter].nor);
            }
            glVertex3fv((GLfloat *)&mesh->verticies[vertexCounter].pos);

            if (g->lighting) {
                glNormal3fv((GLfloat *)&mesh->verticies[vertexCounter + nCols].nor);
            }
            glVertex3fv((GLfloat *)&mesh->verticies[vertexCounter + nCols].pos);

            vertexCounter++;
        }
        glEnd();
    }
}

void drawNormalVectors(Global *g, int tess) {
    const float A1 = 0.25, k1 = 2.0 * M_PI, w1 = 0.25;
    const float A2 = 0.25, k2 = 2.0 * M_PI, w2 = 0.25;
    float stepSize = 2.0 / tess;
    vec3f r, n;
    int i, j;
    float t = g->t;

    // Normals
    for (j = 0; j <= tess; j++) {
        for (i = 0; i <= tess; i++) {
            r.x = -1.0 + i * stepSize;
            r.z = -1.0 + j * stepSize;

            n.y = 1.0;
            n.x = - A1 * k1 * cosf(k1 * r.x + w1 * t);
            if (g->waveDim == 2) {
                r.y = A1 * sinf(k1 * r.x + w1 * t);
                n.z = 0.0;
            } else {
                r.y = A1 * sinf(k1 * r.x + w1 * t) + A2 * sinf(k2 * r.z + w2 * t);
                n.z = - A2 * k2 * cosf(k2 * r.z + w2 * t);
            }

            drawVector(&r, &n, 0.05f, true, &cyan);
        }
    }

}


void drawSineWave(Global *g, Mesh *mesh, int tess) {
    int i;
    GLfloat temp[4] = {1.0,  1.0,  1.0, 0.0};
    if (g->lighting) {
        glEnable(GL_LIGHTING);
        glEnable(GL_NORMALIZE);
        for (i = 0; i < 8; i++) {
            if (g->enabledLight[i]) {
                glEnable(lights[i]);
            } else {
                glDisable(lights[i]);
            }
        }
        glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
    } else {
        glDisable(GL_LIGHTING);
    }

    if (g->polygonMode == line) {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    } else {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }

    switch (g->renderType) {
        case ImmediateMode: 
            drawGridImmediateMode(g, mesh, tess); 
            break;
        case VertexArray: 
            drawGridVertexArrays(g, mesh); 
            break;
        case OldDraw: 
            oldDraw(g, tess);
            break;
        case VertexArrayOneMesh: 
            drawGridVertexArraysOneMesh(g, mesh); 
            break;
        case Vbo:
            drawGridVBO(g, mesh);
            break;
        case VboOneMesh:
            drawGridVBOOneMesh(g, mesh);
            break;
        default:
            break;
    }

    if (g->drawNormals) {
        drawNormalVectors(g, tess);
    }
}

void drawAxes(float length) {
    glPushAttrib(GL_CURRENT_BIT);
    glDisable(GL_LIGHTING);
    glBegin(GL_LINES);

    /* x axis */
    glColor3f(1.0, 0.0, 0.0);
    glVertex3f(0,0,0);//-length, 0.0, 0.0);
    glVertex3f(length, 0.0, 0.0);

    /* y axis */
    glColor3f(0.0, 1.0, 0.0);
    glVertex3f(0.0, 0,0);//-length, 0.0);
    glVertex3f(0.0, length, 0.0);

    /* z axis */
    glColor3f(0.0, 0.0, 1.0);
    glVertex3f(0.0, 0.0, 0);//-length);
    glVertex3f(0.0, 0.0, length);

    glEnd();
    glPopAttrib();
}

void drawVector(vec3f *r, vec3f *v, float s, bool normalize, color3f *c) {
    glPushAttrib(GL_CURRENT_BIT);
    glDisable(GL_LIGHTING);
    glColor3fv((GLfloat *)c);
    glBegin(GL_LINES);
    if (normalize) {
        float mag = sqrt(v->x * v->x + v->y * v->y + v->z * v->z);
        v->x /= mag;
        v->y /= mag;
        v->z /= mag;
    }
    glVertex3fv((GLfloat *)r);
    glVertex3f(r->x + s * v->x, r->y + s * v->y, r->z + s * v->z);
    glEnd();
    glPopAttrib();
}
