#include "main.h"
#include "draw.h"
#include "mesh.h"

typedef void (*displayFP)(Camera *c, Mesh *m);

Global g = {    false,  //animate
                0.0,    //t
                0.0,    //lastT
                line,   //polygonMode
                false,  //lgihting
                false,  //drawNormals
                0,      //width
                0,      //height
                8,      //tess
                2,      //waveDim
                0,      //frameCount
                0.0,    //frameRate
                1.0,    //displayStatsInterval
                0,      //lastStatsDisplayT
                1,      //RenderType
                {1,0,0,0,0,0,0,0,0},//enabledLight
                0,      //nVboIds
                0,      //VboIds
                0,      //VboMesh
                0,      //VboOneMesh
                0,      //sleep
                0.0,    //sleepTime
                0,      //consolePM
                1      //displayOSD
           };

bool debug[d_nflags] = { false, false, false, false, false, false }; 
const float milli = 1000.0;

displayFP displayFunc;
static SDL_Window *mainWindow = 0;

bool wantCalculation = false;
bool wantUpdate = false;
bool wantRedisplay = false;

unsigned int updateCount = 0;

void init(Mesh *mesh) {    
    glClearColor(0.0, 0.0, 0.0, 1.0);
    glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
    glEnable(GL_DEPTH_TEST);

    g.tess = 8;
    mesh->tess = 0;           
    mesh->nVerts = 0;
    mesh->nIndicies = 0;
    g.renderType = VertexArray;
    g.sleep = 0;

    initVboData(&g);
    populateMesh(&g, mesh);

    initLights();
}

void displayOSD() {
    char buffer[30];
    char *bufp;
    int w, h;
    int i;


    glPushAttrib(GL_ENABLE_BIT | GL_CURRENT_BIT);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_LIGHTING);

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();

    //Set up orthographic coordinate system to match the window, 
    //i.e. (0,0)-(w,h) 
    SDL_GetWindowSize(mainWindow, &w, &h);
    glOrtho(0.0, w, 0.0, h, -1.0, 1.0);

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    // Render mode
    glColor3f(1.0, 1.0, 0.0);
    glRasterPos2i(10, 80);
    switch (g.renderType) {
        //The empty spaces exists to ensure every render type has the same number of chars to render.
        //Yes, it does has an effect on framerate for CPU limited bottlenecks 
        //less chars -> high framerate
        case ImmediateMode: 
            strcpy(buffer, "Immediate mode");
            break;
        case VertexArray: 
            strcpy(buffer, "VA            ");
            break;
        case OldDraw: 
            strcpy(buffer, "Old draw      ");
            break;
        case VertexArrayOneMesh: 
            strcpy(buffer, "VA - one mesh ");
            break;
        case Vbo:
            strcpy(buffer, "VBO           ");
            break;
        case VboOneMesh:
            strcpy(buffer, "VBO - one mesh");
            break;
    }
    for (bufp = buffer; *bufp; bufp++) {
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *bufp);           
    }

    // Frame rate 
    glColor3f(1.0, 1.0, 0.0);
    glRasterPos2i(10, 60);
    snprintf(buffer, sizeof buffer, "frame rate (f/s):  %5.0f", g.frameRate);      
    for (bufp = buffer; *bufp; bufp++) {
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *bufp);            
    }

    // Frame time 
    glColor3f(1.0, 1.0, 0.0);
    glRasterPos2i(10, 40);
    snprintf(buffer, sizeof buffer, "frame time (ms/f): %5.0f", 1.0 / g.frameRate * milli);
    for (bufp = buffer; *bufp; bufp++) {
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15,  *bufp);
    }
    // Tesselation 
    glColor3f(1.0, 1.0, 0.0);
    glRasterPos2i(10, 20);
    snprintf(buffer, sizeof buffer, "tesselation:       %5d", g.tess);
    for (bufp = buffer; *bufp; bufp++) {
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *bufp);
    }

    glPopMatrix();  // Pop modelview 
    glMatrixMode(GL_PROJECTION);

    glPopMatrix();  // Pop projection 
    glMatrixMode(GL_MODELVIEW);

    glPopAttrib();
}

void consolePM() {
      printf("frame rate (f/s):  %5.0f\n", g.frameRate);
      printf("frame time (ms/f): %5.0f\n", 1.0 / g.frameRate * 1000.0);
      printf("tesselation:       %5d\n", g.tess);
}

void display(Camera *camera, Mesh *mesh) {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);

    glViewport(0, 0, g.width, g.height);

    // General view
    glPushMatrix();
    glRotatef(camera->rotateX, 1.0, 0.0, 0.0);
    glRotatef(camera->rotateY, 0.0, 1.0, 0.0);
    glScalef(camera->scale, camera->scale, camera->scale);
    drawAxes(5.0);
    
    drawSineWave(&g, mesh, g.tess);
    
    glPopMatrix();

    if (g.displayOSD) {
        displayOSD();
    }

    

    SDL_GL_SwapWindow(mainWindow);

    g.frameCount++;
}

void displayMultiView(Camera *camera, Mesh *mesh) {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);

    // Front view
    glPushMatrix();
    glViewport(g.width / 16.0, g.height * 9.0 / 16.0, g.width * 6.0 / 16.0, g.height * 6.0 / 16.0);
    drawAxes(5.0);
    drawSineWave(&g, mesh, g.tess);
    glPopMatrix();

    // Top view
    glPushMatrix();
    glViewport(g.width / 16.0, g.height / 16.0, g.width * 6.0 / 16.0, g.height * 6.0 / 16);
    glRotatef(90.0, 1.0, 0.0, 0.0);
    drawAxes(5.0);
    drawSineWave(&g, mesh, g.tess);
    glPopMatrix();

    // Left view
    glPushMatrix();
    glViewport(g.width * 9.0 / 16.0, g.height * 9.0 / 16.0, g.width * 6.0 / 16.0, g.height * 6.0 / 16.0);
    glRotatef(90.0, 0.0, 1.0, 0.0);
    drawAxes(5.0);
    drawSineWave(&g, mesh, g.tess);
    glPopMatrix();

    // General view
    glPushMatrix();
    glViewport(g.width * 9.0 / 16.0, g.width / 16.0, g.width * 6.0 / 16.0, g.height * 6.0 / 16.0);
#if 0
    glRotatef(30.0, 1.0, 0.0, 0.0);
    glRotatef(-30.0, 0.0, 1.0, 0.0);
#endif
    glRotatef(camera->rotateX, 1.0, 0.0, 0.0);
    glRotatef(camera->rotateY, 0.0, 1.0, 0.0);
    glScalef(camera->scale, camera->scale, camera->scale);
    drawAxes(5.0);
    drawSineWave(&g, mesh, g.tess);
    glPopMatrix();
    
    if (g.displayOSD) {
        displayOSD();
    }

    SDL_GL_SwapWindow(mainWindow);

    g.frameCount++;

    glPopMatrix();
}

void reshape(int w, int h) {
    g.width = w;
    g.height = h;
    glViewport(0, 0, (GLsizei) w, (GLsizei) h); 
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-1.0, 1.0, -1.0, 1.0, -100.0, 100.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

//recalculate the whole mesh data
void recalculate(Mesh *mesh) {
    populateMesh(&g, mesh);
    printf("Updatecount %u\n", updateCount++);
}

//update for animation
void update(Mesh *mesh) {
    updateMesh(&g, mesh);
}

/* dt in ms */
void sleep(float dt) {
    struct timespec req;
    struct timespec rem;

    if (dt <= 0) {
        return;
    }

    req.tv_sec = 0;
    req.tv_nsec = dt * 1000000;
    rem.tv_sec = 0;
    rem.tv_nsec = 0;

    nanosleep(&req, &rem);
}

void idle() {
    float t, dt;
    float sleepT;

    t = SDL_GetTicks() / milli;

    // Accumulate time if animation enabled
    if (g.animate) {
        dt = t - g.lastT; 
        g.t += dt;
        g.lastT = t;
        if (debug[d_animation])
            printf("idle: animate %f\n", g.t);
        wantUpdate = true;
    }

    // Update stats, although could make conditional on a flag set interactively
    dt = (t - g.lastStatsDisplayT);
    if (dt > g.displayStatsInterval) {
        g.frameRate = g.frameCount / dt;
        if (debug[d_OSD])
            printf("dt %f framecount %d framerate %f\n", dt, g.frameCount, g.frameRate);
        g.lastStatsDisplayT = t;
        g.frameCount = 0;
        if (g.consolePM) {
            consolePM();
        }
    }
    if (g.sleep) {
        sleepT = ((1.0/30.0) * milli) - ((t - g.sleepTime) * milli);
        if (sleepT < 0) {
            sleepT = 0;
        }
        sleep(sleepT);
    }
    g.sleepTime = SDL_GetTicks() / milli;

    wantRedisplay = 1;
}

void keyboard(SDL_Keycode key) {
    switch (key) {
    case SDLK_ESCAPE:
        printf("exit\n");
        exit(0);
        break;
    case SDLK_a:
        g.animate = !g.animate;
        if (g.animate) {
            g.lastT = SDL_GetTicks() / milli;
        } 
        break;
    case SDLK_l:
        g.lighting = !g.lighting;
        wantRedisplay = true;
        wantCalculation = true;
        break;
    case SDLK_m:
        printf("%d\n", g.polygonMode);
        if (g.polygonMode == line)
            g.polygonMode = fill;
        else
            g.polygonMode = line;
        wantRedisplay = true;
        break;
    case SDLK_n:
        g.drawNormals = !g.drawNormals;
        wantRedisplay = true;
        break;
    case SDLK_s:
        displayFunc = &display;
        wantRedisplay = true;
        break;
    case SDLK_x: 
        displayFunc = &displayMultiView;
        wantRedisplay = true;
        break;
    case SDLK_KP_PLUS:
        g.tess *= 2;
        wantRedisplay = true;
        wantCalculation = true;
        break;
    case SDLK_KP_MINUS:
        g.tess /= 2;
        if (g.tess < 8)
            g.tess = 8;
        wantRedisplay = true;
        wantCalculation = true;
        break;
    case SDLK_d:
        g.waveDim++;
        if (g.waveDim > 3)
            g.waveDim = 2;
        wantCalculation = true;
        break;
    case SDLK_i:
        g.renderType++;
        if (g.renderType > 5) {
            g.renderType = 0;
        }
        wantRedisplay = true;
        wantCalculation = true;
        break;
    case SDLK_f:
        g.sleep = !g.sleep;
        wantRedisplay = true;
        break;
    case SDLK_c:
        g.consolePM = !g.consolePM;
        wantRedisplay = true;
        break;
    case SDLK_o:
        g.displayOSD = !g.displayOSD;
        wantRedisplay = true;
        break;
    case SDLK_0:
        g.enabledLight[0] = !g.enabledLight[0];
        wantRedisplay = true;
        break;
    case SDLK_1:
        g.enabledLight[1] = !g.enabledLight[1];
        break;
    case SDLK_2:
        g.enabledLight[2] = !g.enabledLight[2];
        break;
    case SDLK_3:
        g.enabledLight[3] = !g.enabledLight[3];
        break;
    case SDLK_4:
        g.enabledLight[4] = !g.enabledLight[4];
        break;
    case SDLK_5:
        g.enabledLight[5] = !g.enabledLight[5];
        break;
    case SDLK_6:
        g.enabledLight[6] = !g.enabledLight[6];
        break;
    case SDLK_7:
        g.enabledLight[7] = !g.enabledLight[7];
        break;
    default:
        break;
    }
}

void motion(Camera *camera, int x, int y) {
    float dx, dy;

    if (debug[d_mouse]) {
        printf("motion: %d %d\n", x, y);
        printf("camera->rotate: %f %f\n", camera->rotateX, camera->rotateY);
        printf("camera->scale:%f\n", camera->scale);
    }

    dx = x - camera->lastX;
    dy = y - camera->lastY;
    camera->lastX = x;
    camera->lastY = y;

    switch (camera->control) {
    case inactive:
        break;
    case rotate:
        camera->rotateX += dy;
        camera->rotateY += dx;
        break;
    case pan:
        break;
    case zoom:
        camera->scale += dy / 100.0;
        break;
    }

    wantRedisplay = true;
}

void mouse(Camera *camera, unsigned int button, unsigned int state, int x, int y) {
    if (debug[d_mouse])
        printf("mouse: %d %d %d\n", button, x, y);

    camera->lastX = x;
    camera->lastY = y;

    if (state == SDL_MOUSEBUTTONDOWN)
        switch(button) {
        case SDL_BUTTON_LEFT:
            camera->control = rotate;
            break;
        case SDL_BUTTON_MIDDLE:
            camera->control = pan;
            break;
        case SDL_BUTTON_RIGHT:
            camera->control = zoom;
            break;
    }
    else if (state == SDL_MOUSEBUTTONUP)
        camera->control = inactive;
}

bool handleEvents(Camera *camera) {
    SDL_Event event;

    while (SDL_PollEvent(&event)) {
        switch (event.type) {
        case SDL_MOUSEMOTION:
            motion(camera, event.motion.x, event.motion.y);
            break;
        case SDL_MOUSEBUTTONDOWN:
            mouse(camera, event.button.button, SDL_MOUSEBUTTONDOWN, event.button.x, event.button.y);
            break;
        case SDL_MOUSEBUTTONUP:
            mouse(camera, event.button.button, SDL_MOUSEBUTTONUP, event.button.x, event.button.y);
            break;
        case SDL_KEYDOWN:
            keyboard(event.key.keysym.sym);
            break;
        case SDL_WINDOWEVENT:
            switch (event.window.event) {
            case SDL_WINDOWEVENT_RESIZED:
                if (event.window.windowID == SDL_GetWindowID(mainWindow)) {
                    SDL_SetWindowSize(mainWindow, event.window.data1, event.window.data2);
                    reshape(event.window.data1, event.window.data2);
                }
                break;
            case SDL_WINDOWEVENT_CLOSE:
                return true;
                break;
            default:
                break;  
            }
            break;

        default:
            break;
        }
    }

    return false;
}

void mainloop(Camera *camera, Mesh *mesh) {
    while (1) {
        if (handleEvents(camera)) {
            exit(-1);
        }
        idle();
        if (wantCalculation) {
            recalculate(mesh);
            wantCalculation = 0;
        } else if (wantUpdate) {
            update(mesh);
            wantUpdate = 0;
        }
        if (wantRedisplay) {
            displayFunc(camera, mesh);
            wantRedisplay = 0;
        }
    }
}

void initGraphics() {
    SDL_GLContext mainGLContext;

    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);

    mainWindow = SDL_CreateWindow("Assignment 1", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 1920, 1080, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);

    if (mainWindow == NULL) {
        fprintf(stderr, "Failed to create window: %s\n", SDL_GetError());
        exit(EXIT_SUCCESS);
    }

    mainGLContext = SDL_GL_CreateContext(mainWindow);
    SDL_GL_MakeCurrent(mainWindow, mainGLContext);
}

int main(int argc, char** argv) {
	   Camera camera = { 0, 0, 30.0, -30.0, 1.0, inactive };
	   Mesh mesh;
	
    glutInit(&argc, argv);
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        fprintf(stderr, "Unable to init SDL: %s\n", SDL_GetError());
        return EXIT_SUCCESS;
    }

    initGraphics();

    init(&mesh);
    displayFunc = &display;

    mainloop(&camera, &mesh);
    return 0;
}
