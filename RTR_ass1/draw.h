#ifndef H_DRAW
#define H_DRAW

#include "mesh.h"

typedef struct { float r, g, b; } color3f;

void initLights();
void drawNormalVectors(Global *g, int tess);
void drawSineWave(Global *g, Mesh *mesh, int tess);
void drawAxes(float length);
void drawVector(vec3f *r, vec3f *v, float s, bool normalize, color3f *c); 

#endif
