#ifndef H_MAIN
#define H_MAIN

#include <stdio.h>
#include <stdbool.h>
#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <SDL2/SDL.h>
#include <time.h>

#include <stdio.h>
#include <math.h>


typedef enum {
    d_drawSineWave,
    d_mouse,
    d_key,
    d_animation,
    d_lighting,
    d_OSD,
    d_nflags
} DebugFlags;


typedef enum { line, fill } polygonMode_t;          
typedef enum {  OldDraw, 
                ImmediateMode, 
                VertexArray, 
                VertexArrayOneMesh,
                Vbo,
                VboOneMesh} RenderType;

typedef struct globals {
    bool animate;                
    float t, lastT;
    polygonMode_t polygonMode;
    bool lighting;              
    bool drawNormals;            
    int width, height;
    int tess;
    int waveDim;
    int frameCount;
    float frameRate;
    float displayStatsInterval;
    int lastStatsDisplayT;
    RenderType renderType;
    int enabledLight[9];
    int nVboIds;
    GLuint *VboIds;
    GLuint *VboMesh;
    GLuint *VboOneMesh;
    bool sleep;                  
    float sleepTime;
    bool consolePM;              
    bool displayOSD;             
} Global;

typedef enum { inactive, rotate, pan, zoom } CameraControl;

typedef struct {
    int lastX, lastY;
    float rotateX, rotateY;
    float scale;
    CameraControl control;
} Camera;

#endif // !H_MAIN
