#ifndef H_COLLISION
#define H_COLLISION

#include "main.h"
#include "particle.h"
#include "arena.h"

void collideParticlesUniformGrid(ParticleCloud *particleCloud,  Arena *arenaa, int dt);
#endif