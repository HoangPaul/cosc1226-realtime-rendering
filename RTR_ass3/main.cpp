#include "main.h"
#include "particle.h"
#include "arena.h"
#include "collision.h"
#include "shaders.h"
#include "UI.h"
#include "display.h"

/* Changable parameters */

const int   MAX_NUM_PARTICLES = 1000;
const int   ARENA_SIZE = 30;
const bool  ARENA_IS_CIRCLE = true;


/* End changable parameters */

Global global;

/* Rendering info. */
enum renderMode { wire, solid };
static renderMode renMode = solid;

bool isGravity = true;
GLuint shaderProgram = 0;
GLuint shaderProgram2 = 0;
GLuint currShaderProgram = 0;

GameState game_state;

UIButton testButton;

void restart(void);

void panic(const char *m) {
    printf("%s", m);
    system("pause");
    exit(1);
}

glm::vec2 mouseLocToWorldLoc(Screen *screen, int x, int y) {
    glm::vec2 world_vec = glm::vec2(0.0);
    world_vec.x = (x / (float)screen->windowWidth - 0.5) * screen->zoom * 10;
    world_vec.y = -(y / (float)screen->windowHeight - 0.5) * screen->zoom * 10;

    return world_vec;
}

void setRenderMode(renderMode rm) {
    if (rm == wire) {
        glDisable(GL_LIGHTING);
        glDisable(GL_DEPTH_TEST);
        glDisable(GL_NORMALIZE);
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    } else if (rm == solid) {
        //glEnable(GL_LIGHTING);
        //glEnable(GL_LIGHT0);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_NORMALIZE);
        glShadeModel(GL_SMOOTH);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void changeRenderMode(void) {
    if (renMode == wire) {
        renMode = solid;
    } else {
        renMode = wire;
    }
    printf("render mode is %d\n", renMode);
    setRenderMode(renMode);
}


void checkVictoryConditions(void) {
    Particle *player = global.player;

    if (player->mass <= 0) {
        game_state = Lose;
    } else {
        Particle *particles = global.particleCloud->particles;
        float totalMass = 0;
        for (int i = 0; i < global.particleCloud->curNumParticles; i++) {
            totalMass += particles[i].mass;
        }
        if (player->mass > totalMass / 2.0) {
            game_state = Win;
        }
    }
}


void update(Screen *screen, Input *input, Time *time) {
    if (game_state != Play) return;

    if (input->restart) {
        restart();
    }
    if (input->gravity) {
        isGravity = !isGravity;
    }
    if (input->shader) {
        if (currShaderProgram == shaderProgram) {
            currShaderProgram = shaderProgram2;
        } else {
            currShaderProgram = shaderProgram;
        }
        
    }
    if (input->renderMode) {
        changeRenderMode();
    }
    if (input->mouse.isLeftClick) {
        glm::vec2 world_vec = mouseLocToWorldLoc(screen, input->mouse.px_mouseX, input->mouse.px_mouseY);
        moveParticle(global.player, world_vec, time->dt);
    }
    if (input->mouse.isRightClick) {
        glm::vec2 world_vec = mouseLocToWorldLoc(screen, input->mouse.px_mouseX, input->mouse.px_mouseY);
        blackhole(global.particleCloud, world_vec, isGravity, time->dt);
    }


    moveEnemy(global.particleCloud, global.enemy, time->dt, ARENA_SIZE);
    updateParticles(global.particleCloud, global.arena, time->dt);
}

void display(Screen *screen, Time *time) {
    static int frameNo = 0;

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    switch (game_state) {
    case MainMenu:
        showStateUI(game_state);
        showOSD(time, frameNo);
        break;
    case Pause:
        showStateUI(game_state);
        showOSD(time, frameNo); 
        displayGameScene(&global, currShaderProgram, screen->zoom);
        break;
    case Play:
        showStateUI(game_state);
        showOSD(time, frameNo);
        displayGameScene(&global, currShaderProgram, screen->zoom);
    default:
        showStateUI(game_state);
        showOSD(time, frameNo);
        displayGameScene(&global, currShaderProgram, screen->zoom);
        break;
    }

    SDL_GL_SwapWindow(screen->mainWindow);
    frameNo++;
}

void reshape(Screen *screen, int w, int h) {
    screen->windowWidth = w;
    screen->windowHeight = h;
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glOrtho(-screen->zoom, screen->zoom, -screen->zoom, screen->zoom, -1.0, 1.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void cleanup(void) {

}

void initShaders() {
    static bool hasLoadedShaders = false;

    if (hasLoadedShaders) return;

    shaderProgram = getShader("sparkly.vert", "sparkly.frag");
    shaderProgram2 = getShader("shader.vert", "shader.frag");
    currShaderProgram = shaderProgram2;
    hasLoadedShaders = true;
}

void myInit(void) {
    setRenderMode(renMode);

    srand(time(NULL));
    initShaders();

    initArena(&global, ARENA_SIZE, ARENA_IS_CIRCLE);
    initParticleCloud(&global, MAX_NUM_PARTICLES);
    initialiseParticlesRandomly(&global, global.arena);


    testButton.height = 0.1;
    testButton.length = 0.2;
    testButton.pos.x = 0.1;
    testButton.pos.y = 0.05;
    strcpy(testButton.text, "Hello");

    printf("qdwdqdwq\n"); 

    game_state = MainMenu;
}

void restart() {
    cleanupArena(&global);
    cleanUpParticleCloud(&global);

    myInit();
}
