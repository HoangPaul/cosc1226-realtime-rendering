#include "UI.h"

void displayTextCentred(UIButton *button, int px_screenWidth, int px_screenHeight);

void displayButtonUI(UIButton *button, float px_screenWidth, float px_screenHeight) {
    //Draw box
    glBegin(GL_LINE_LOOP);
    glColor3f(1.0, 1.0, 1.0);
    glVertex3f(button->pos.x, button->pos.y, 0.0);
    glVertex3f(button->pos.x, button->pos.y + button->height, 0.0);
    glVertex3f(button->pos.x + button->length, button->pos.y + button->height, 0.0);
    glVertex3f(button->pos.x + button->length, button->pos.y, 0.0);
    glEnd();

    displayTextCentred(button, px_screenWidth, px_screenHeight);
}

/** Draw text inside box */
void displayTextCentred(UIButton *button, int px_screenWidth, int px_screenHeight) {
    int textLength = strlen(button->text);
    float textLengthOffset = ((float)textLength) * 9.0 / ((float)px_screenWidth) / 2;
    float textHeightOffset =  15.0 / ((float)px_screenHeight) / 4.0; //15px high
    int i;

    glPushAttrib(GL_ENABLE_BIT | GL_CURRENT_BIT);
    glRasterPos2f(button->pos.x + button->length / 2 - textLengthOffset, button->pos.y + button->height / 2 + textHeightOffset);
    for (i = 0; i < textLength; i++) {
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15, button->text[i]);
    }
    glPopAttrib();
}

bool isPointWithinButton(UIButton *button, int px_x, int px_y, int px_screenWidth, int px_screenHeight) {
    float px_btnPosX = button->pos.x * ((float)px_screenWidth);
    float px_btnPosY = button->pos.y * ((float)px_screenHeight);
    float px_btnLenX = button->length * ((float)px_screenWidth);
    float px_btnLenY = button->height * ((float)px_screenHeight);

    bool ans = px_btnPosX < px_x && px_btnPosY < px_y &&
        (px_btnPosX + px_btnLenX) > px_x && (px_btnPosY + px_btnLenY) > px_y;

    printf("is within: %d, x:%d, y:%d %f %f %f %f\n", ans, px_x, px_y, px_btnPosX, px_btnPosY, px_btnLenX, px_btnLenY);

    return  ans;
}