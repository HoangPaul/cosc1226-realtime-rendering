#ifndef H_UI
#define H_UI
#define MAX_UI_TEXT_LENGTH 30

#include "main.h"

struct UIButton {
    glm::vec2 pos;
    float length;
    float height;
    char text[MAX_UI_TEXT_LENGTH];
    void(*onClick)(void);
};

void displayButtonUI(UIButton *button, float px_screenWidth, float px_screenHeight);
bool isPointWithinButton(UIButton *button, int px_x, int px_y, int px_screenWidth, int px_screenHeight);

#endif