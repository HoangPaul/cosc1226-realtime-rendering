#ifndef SDLBASE_H
#define SDLBASE_H

#define _CRT_SECURE_NO_WARNINGS
#define GL_GLEXT_PROTOTYPES

#if _WIN32
#include <Windows.h>
#include <GL/glew.h>
#endif
#include <stdio.h>
#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <SDL2/SDL.h>
#include <time.h>
#include <limits.h>
#include <stdlib.h>

#include <stdio.h>
#include <math.h>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/constants.hpp>

struct Time {
    float elapsedTime; 
    float startTime;
    float lastTime;
    float dt;
};

struct Screen {
    int windowWidth;
    int windowHeight;
    SDL_Window *mainWindow;
    float zoom;
};

struct Mouse {
    int px_mouseX;
    int px_mouseY;
    bool isLeftClick;
    bool isRightClick;
};

struct Input {
    Mouse mouse;
    bool gravity;
    bool shader;
    bool restart;
    bool renderMode;
};

glm::vec2 mouseLocToWorldLoc(Screen *screen, int x, int y);

void myInit(void);
void update(Screen *screen, Input *input, Time *time);
void display(Screen *screen, Time *time);
void reshape(Screen *screen, int w, int h);
void cleanup(void);

#endif