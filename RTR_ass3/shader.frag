uniform float size;

varying vec3 pos;

void main (void)
{   
    vec4 colour = gl_Color;    
    colour.w = sqrt(pos.x * pos.x + pos.y * pos.y) / abs(size * size);
    gl_FragColor = colour;
}