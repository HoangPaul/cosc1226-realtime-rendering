#ifndef H_PARTICLE
#define H_PARTICLE

#include "main.h"

typedef enum ParticleType {Normal, Player, Enemy};

struct Particle {
    glm::vec2 position;
    glm::vec2 velocity;
    float radius;
    float mass;
    float elasticity;
    GLUquadric *quadric;  /* For rendering. */
    int slices, loops;    /* For rendering. */
    int indexInCloud;
    ParticleType particleType;
    bool isDead;
};

struct ParticleCloud {
    Particle *particles;
    int maxNumParticles;
    int curNumParticles;
};

void initialiseParticlesRandomly(Global *g, Arena *arena);
void initParticleCloud(Global *g, int numParticles);
void cleanUpParticleCloud(Global *g);
void integrateMotionParticles(ParticleCloud *particleCloud, float dt);
inline bool collisionDetectionParticles(Particle &p1, Particle &p2);
void eulerStepSingleParticle(Particle &p, float dt);
void displayParticles(Global *global, GLuint shader);
boolean isParticleDead(Particle *particle);
void killParticleFromCloud(ParticleCloud *particleCloud, Particle *particle);
void moveParticle(Particle *particle, glm::vec2 worldPos, float dt);
void moveEnemy(ParticleCloud *particleCloud, Particle *particle, float dt, float searchRange);
void updateParticles(ParticleCloud *particleCloud, Arena *arena, float dt);
void blackhole(ParticleCloud *particleCloud, glm::vec2 world_vec, bool isGravity, float dt);


float getMassFromRadius(float radius);
float getRadiusFromMass(float mass);

#endif