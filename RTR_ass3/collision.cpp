#include "collision.h"
inline void calcGridIndex(Particle &p, Arena a,
    float gridCellSize[2], int gridNumCells[2],
    int index[2]) {
    index[0] = (int)((p.position[0] - a.min[0]) / gridCellSize[0]);
    index[1] = (int)((p.position[1] - a.min[1]) / gridCellSize[1]);


}


inline bool collisionDetectionParticles(Particle &p1, Particle &p2) {
    float sum_radii, sum_radii_sq, n[2], n_mag_sq;

    sum_radii = p1.radius + p2.radius;
    sum_radii_sq = sum_radii * sum_radii;
    n[0] = p2.position[0] - p1.position[0];
    n[1] = p2.position[1] - p1.position[1];
    n_mag_sq = n[0] * n[0] + n[1] * n[1];

    return n_mag_sq <= sum_radii_sq;
}


void collisionReactionParticles2DprojNormal(Particle &p1, Particle &p2)
{
    float  n[2], n_mag;
    float projnv1, projnv2;
    float m1, m2, v1i, v2i, v1f, v2f;

    /* Normal vector n between centres. */
    n[0] = p2.position.x - p1.position.x;
    n[1] = p2.position.y - p1.position.y;

    /* Normalise n. */
    n_mag = sqrt(n[0] * n[0] + n[1] * n[1]);
    n[0] /= n_mag;
    n[1] /= n_mag;

    /* Vector projection/component/resolute of velocity in n direction. */
    projnv1 = n[0] * p1.velocity.x + n[1] * p1.velocity.y;
    projnv2 = n[0] * p2.velocity.x + n[1] * p2.velocity.y;

    /* Use 1D equations to calculate final velocities in n direction. */
    v1i = projnv1;
    v2i = projnv2;
    m1 = p1.mass;
    m2 = p2.mass;
    v1f = (m1 - m2) / (m1 + m2) * v1i + 2.0 * m2 / (m1 + m2) * v2i;
    v2f = 2.0 * m1 / (m1 + m2) * v1i + (m2 - m1) / (m1 + m2) * v2i;

    /* Vector addition to solve for final velocity. */
    p1.velocity.x = (p1.velocity.x - v1i * n[0]) + v1f * n[0];
    p1.velocity.y = (p1.velocity.y - v1i * n[1]) + v1f * n[1];
    p2.velocity.x = (p2.velocity.x - v2i * n[0]) + v2f * n[0];
    p2.velocity.y = (p2.velocity.y - v2i * n[1]) + v2f * n[1];
}

void collideAbsorb(Particle *p1, Particle *p2) {
    Particle *big;
    Particle *sml;

    if (p1->mass < p2->mass) {
        big = p2;
        sml = p1;
    }
    else if (p1->mass > p2->mass) {
        big = p1;
        sml = p2;
    }
    else {
        /* They exactly the same? as floating points?! */
        if (p1 == p2) {
            panic("Comparing same particle as collision");
        }
        printf("Special occasion! same particle size\n");
        collisionReactionParticles2DprojNormal(*p1, *p2);
        return;
    }


    float distFromCentre = glm::length(big->position - sml->position);
    float overlapAmount = (big->radius + sml->radius) - distFromCentre;

    if (overlapAmount < 0) {
        panic("These should not be colliding");
    }

    float changeInMass = 0.0f;

    /* Change  mass to prevserve momentum*/
    if (overlapAmount > sml->radius) {
        //printf("Total absorbption\n");
        big->mass += sml->mass;
        changeInMass = sml->mass;
        sml->mass = 0;
    }
    else {
        changeInMass = getMassFromRadius(overlapAmount);
        big->mass += changeInMass;
        sml->mass -= changeInMass;
    }

    /* Change velocity to preseve momentum*/
    big->velocity.x = (big->mass * big->velocity.x + changeInMass * sml->velocity.x) / (big->mass + changeInMass);
    big->velocity.y = (big->mass * big->velocity.y + changeInMass * sml->velocity.x) / (big->mass + changeInMass);

    big->radius = getRadiusFromMass(big->mass);
    sml->radius = getRadiusFromMass(sml->mass);

    if (sml->mass <= 0) {
        sml->isDead = true;
    }
}

void collideParticlesBruteForce(ParticleCloud *particleCloud, Arena *arenaa, int dt) {
    int numParticles = particleCloud->curNumParticles;
    Particle *particle = particleCloud->particles;
    Arena arena = *arenaa;
    int i, j;

    for (i = 0; i < numParticles - 1; i++) {
        for (j = i + 1; j < numParticles; j++) {
            if (collisionDetectionParticles(particle[i], particle[j])) {
                /* Collision */
                collideAbsorb(&particle[i], &particle[j]);
            }
        }
    }
}

bool hasShownMessage = false;

void collideParticlesUniformGrid(ParticleCloud *particleCloud, Arena *arenaa, int dt) {
    float gridCellSize[2];
    int **gridCellParticleCount, **gridCellParticleListEnd, *gridCellParticleList;
    int gridNumCells[2], gridSize, gridIndex[2], gridCellParticleListStart;
    int gridIndexMin[2], gridIndexMax[2];
    int i, j, k, s, t, p1, p2, total;

    int numParticles = particleCloud->curNumParticles;
    Particle *particle = particleCloud->particles;
    Arena arena = *arenaa;

    float largest = 0;

    for (i = 0; i < numParticles; i++) {
        if (particle[i].radius > largest) {
            largest = particle[i].radius;
        }
    }

    /* Work out grid dimensions and allocate. */
    gridNumCells[0] = (int)(sqrt((float)numParticles) + 1);
    gridNumCells[1] = (int)(sqrt((float)numParticles) + 1);
    gridCellSize[0] = (arena.max[0] - arena.min[0]) / gridNumCells[0];
    gridCellSize[1] = (arena.max[1] - arena.min[1]) / gridNumCells[1];
    gridSize = gridNumCells[0] * gridNumCells[1];

    /* Assumption. */
    for (i = 0; i < numParticles; i++)
        if (particle[i].radius * 2.0 > gridCellSize[0] ||
            particle[i].radius * 2.0 > gridCellSize[1]) {
        /*Gee, this is a pain. We can either recalculate the grid size to continue using
         based collision detection or we can switch to brute force. The path of least resistance.
         */
        if (!hasShownMessage) {
            printf("Switching to brute force now.\n");
            hasShownMessage = true;
        }
        collideParticlesBruteForce(particleCloud, arenaa, dt);
        return;
        }

    /* Allocate arrays. */
    gridCellParticleCount = (int **)malloc(gridNumCells[0] * sizeof(int *));
    if (gridCellParticleCount == 0)
        panic("collideParticlesUniformGrid: malloc failed\n");
    gridCellParticleListEnd = (int **)malloc(gridNumCells[0] * sizeof(int *));
    if (gridCellParticleListEnd == 0)
        panic("collideParticlesUniformGrid: malloc failed\n");
    for (i = 0; i < gridNumCells[0]; i++) {
        gridCellParticleCount[i] = (int *)malloc(gridNumCells[1] * sizeof(int));
        if (gridCellParticleCount[i] == 0)
            panic("collideParticlesUniformGrid: malloc failed\n");
        gridCellParticleListEnd[i] = (int *)malloc(gridNumCells[1] * sizeof(int));
        if (gridCellParticleListEnd[i] == 0)
            panic("collideParticlesUniformGrid: malloc failed\n");
    }
    gridCellParticleList = (int *)malloc(numParticles * sizeof(int));

    /* Initialise grid particle count. */
    for (i = 0; i < gridNumCells[0]; i++)
        for (j = 0; j < gridNumCells[1]; j++)
            gridCellParticleCount[i][j] = 0;

    /* Cell counts. */
    for (i = 0; i < numParticles; i++) {
        calcGridIndex(particle[i], arena, gridCellSize, gridNumCells, gridIndex);
        gridCellParticleCount[gridIndex[0]][gridIndex[1]] += 1;
    }

    /* Work out end of cell lists by accumulating cell counts. */
    for (i = 0; i < gridNumCells[0]; i++)
        for (j = 0; j < gridNumCells[1]; j++)
            gridCellParticleListEnd[i][j] = 0;

    total = 0;
    for (i = 0; i < gridNumCells[0]; i++)
        for (j = 0; j < gridNumCells[1]; j++) {
        total = total + gridCellParticleCount[i][j];
        gridCellParticleListEnd[i][j] = total - 1;
        }

    /* Build particle lists. */
    for (i = 0; i < gridNumCells[0]; i++)
        for (j = 0; j < gridNumCells[1]; j++)
            gridCellParticleCount[i][j] = 0;

    for (i = 0; i < numParticles; i++) {
        calcGridIndex(particle[i], arena, gridCellSize, gridNumCells, gridIndex);
        gridCellParticleList[gridCellParticleListEnd[gridIndex[0]][gridIndex[1]] - gridCellParticleCount[gridIndex[0]][gridIndex[1]]] = i;
        gridCellParticleCount[gridIndex[0]][gridIndex[1]] += 1;
    }


    /* Collision detection. */
    for (i = 0; i < numParticles; i++) {
        calcGridIndex(particle[i], arena, gridCellSize, gridNumCells, gridIndex);

        /* Grid index bounds for this particle. */
        gridIndexMin[0] = gridIndex[0] - 1;
        if (gridIndexMin[0] < 0)
            gridIndexMin[0] = 0;
        gridIndexMin[1] = gridIndex[1] - 1;
        if (gridIndexMin[1] < 0)
            gridIndexMin[1] = 0;
        gridIndexMax[0] = gridIndex[0] + 1;
        if (gridIndexMax[0] > gridNumCells[0] - 1)
            gridIndexMax[0] = gridNumCells[0] - 1;
        gridIndexMax[1] = gridIndex[1] + 1;
        if (gridIndexMax[1] > gridNumCells[1] - 1)
            gridIndexMax[1] = gridNumCells[1] - 1;

        p1 = i;

        for (s = gridIndexMin[0]; s <= gridIndexMax[0]; s++) {
            for (t = gridIndexMin[1]; t <= gridIndexMax[1]; t++) {
                gridCellParticleListStart =
                    gridCellParticleListEnd[s][t] - gridCellParticleCount[s][t] + 1;
                for (j = gridCellParticleListStart;
                    j <= gridCellParticleListEnd[s][t];
                    j++) {
                    p2 = gridCellParticleList[j];

                    /* Don't test particle against itself. */
                    if (p2 == p1)
                        continue;

                    /* Only test pairs once. */
                    if (p2 < p1)
                        continue;


                    if (collisionDetectionParticles(particle[p1], particle[p2])) {
                        collideAbsorb(&particle[p1], &particle[p2]);

                    }
                }
            }

        }
    }

    /* Free arrays. */
    for (i = 0; i < gridNumCells[0]; i++) {
        free(gridCellParticleCount[i]);
        free(gridCellParticleListEnd[i]);
    }
    free(gridCellParticleCount);
    free(gridCellParticleListEnd);
    free(gridCellParticleList);
}