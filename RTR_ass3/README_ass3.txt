Paul Hoang
3383494

-Tested in Windows Visual Studio 2012
-Untested on Sutherland Labs (the suspense is killing me).

Two shader programs are available. One has transparency and the other has a 
sparkly effect using pseudorandom. They both do direct manipulation of the colours.

Two arenas available. To change arenas, you must change the bool variable at the 
top of main.cm and recompile. When playing on circular arena, it's recommended that 
filled mode is turned on for visual impact.

The player controlled mote does not eject mass to move (it just applies an 
acceleration), which also means that it is not a closed system.

The program will use brute force search only when the grid search throws an error.

There's also an AI controlled mote (yellow) using simple steering behaviours. It 
will try to move towards smaller motes.

Right click will create a graviational pull on the cursor. It can be toggled 
between pull/push by pressing 'g'.

Win condition  - Have a mass > total_mass/2
Lose Condition - Mass = 0 


Controls:
s - start
w - fill/wire mode
r - change shader program
q - quit
KP+ - Zoom in
KP- - Zoom out
left click - move mote in opposite direction
right click - use gravity
g - toggle gravity/anti-gravity