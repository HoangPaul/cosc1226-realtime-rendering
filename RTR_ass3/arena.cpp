#include "arena.h"
#include "particle.h"

void collideParticleSqaureWall(Particle &p, Arena &a);
void collideParticleCircleWall(Particle &p, Arena &a);
void collideParticlesSquareWall(ParticleCloud *particleCloud, Arena *arena);
void collideParticlesCircleWall(ParticleCloud *particleCloud, Arena *arena);

void initArena(Global *g, int size, bool isCircleArena) {
    g->arena = (Arena*)calloc(1, sizeof(Arena));
    g->arena->isCircleArena = isCircleArena;

    if (g->arena->isCircleArena) {    
        g->arena->centre = glm::vec2(0.0);
        g->arena->radius = 15;
    }
    g->arena->min.x = -size;
    g->arena->min.y = -size;
    g->arena->max.x = size;
    g->arena->max.y = size;

    g->arena->momentum.x = 0.0;
    g->arena->momentum.y = 0.0;
}

void cleanupArena(Global *global) {
    free(global->arena);
}

void displayArena(Arena *arena) {
    if (arena->isCircleArena) {
        glColor3f(1.0, 1.0, 1.0);
        GLUquadric *quadric = gluNewQuadric();
        glPushMatrix();
        glTranslatef(0,0,1);
        gluDisk(quadric, 0, arena->radius, 50, 1);
        glPopMatrix();
    } else {
        glBegin(GL_LINE_LOOP);
        glColor3f(1.0, 1.0, 1.0);
        glVertex3f(arena->min.x, arena->min.y, 0.0);
        glVertex3f(arena->max.x, arena->min.y, 0.0);
        glVertex3f(arena->max.x, arena->max.y, 0.0);
        glVertex3f(arena->min.x, arena->max.y, 0.0);
        glEnd();
    }
}

void collideParticlesWall(ParticleCloud *particleCloud, Arena *arena) {
    if (arena->isCircleArena) {
        collideParticlesCircleWall(particleCloud, arena);
    } else {
        collideParticlesSquareWall(particleCloud, arena);
    }
}

void collideParticlesSquareWall(ParticleCloud *particleCloud, Arena *arena) {
    for (int i = 0; i < particleCloud->curNumParticles; i++) {
        collideParticleSqaureWall(particleCloud->particles[i], *arena);
    }
}

void collideParticlesCircleWall(ParticleCloud *particleCloud, Arena *arena) {
    for (int i = 0; i < particleCloud->curNumParticles; i++) {
        collideParticleCircleWall(particleCloud->particles[i], *arena);
    }
}

void collideParticleSqaureWall(Particle &p, Arena &a) {
    float dp[2];

    dp[0] = dp[1] = 0.0;
    if ((p.position.x - p.radius) < a.min.x) {
        p.position.x += 2.0 * (a.min.x - (p.position.x - p.radius));
        p.velocity.x *= -1.0;
        dp[0] += p.mass * -2.0 * p.velocity.x;
    }
    if ((p.position.y - p.radius) < a.min.y) {
        p.position.y += 2.0 * (a.min.y - (p.position.y - p.radius));
        p.velocity.y *= -1.0;
        dp[1] += p.mass * -2.0 * p.velocity.y;
    }
    if ((p.position.x + p.radius) > a.max.x) {
        p.position.x -= 2.0 * (p.position.x + p.radius - a.max.x);
        p.velocity.x *= -1.0;
        dp[0] += p.mass * -2.0 * p.velocity.x;
    }
    if ((p.position.y + p.radius) > a.max.y) {
        p.position.y -= 2.0 * (p.position.y + p.radius - a.max.y);
        p.velocity.y *= -1.0;
        dp[1] += p.mass * -2.0 * p.velocity.y;
    }
    a.momentum.x += dp[0];
    a.momentum.y += dp[1];
}

void collideParticleCircleWall(Particle &p, Arena &a) {
    glm::vec2 centre = a.centre;
    
    if (glm::distance(p.position, centre) + p.radius > a.radius) {
        /* collision */
        glm::vec2 normal = centre - p.position;
        glm::vec2 initVelocity = p.velocity;
        float mag = glm::length(initVelocity);
        glm::vec2 finalVelocity =  glm::reflect(initVelocity, normal);
        finalVelocity = glm::normalize(finalVelocity) * mag;
        
        /* A little jumpy but it works*/
        p.position += glm::normalize(finalVelocity);
        
        p.velocity = finalVelocity;
    }
}