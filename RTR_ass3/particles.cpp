#include "main.h"
#include "particle.h"
#include "collision.h"
#include "shaders.h"

float random_uniform() {
    return rand()/(float)RAND_MAX;
}

float getRadiusFromMass(float mass) {
    return sqrt(mass);
}

float getMassFromRadius(float radius) {
    return radius * radius;
}

void initParticleCloud(Global *g, int numParticles) {
    g->particleCloud = (ParticleCloud*)calloc(1, sizeof(ParticleCloud));
    g->particleCloud->particles = (Particle*)calloc(numParticles + 1, sizeof(Particle));
    g->particleCloud->maxNumParticles = numParticles + 1;
    g->particleCloud->curNumParticles = g->particleCloud->maxNumParticles;

    g->player = &g->particleCloud->particles[0];
    g->player->particleType = Player;
    g->enemy = &g->particleCloud->particles[1];
    g->enemy->particleType = Enemy;
    
}

void cleanUpParticleCloud(Global *g) {
    free(g->particleCloud->particles);
    free(g->particleCloud);
    g->player = NULL;
    g->enemy = NULL;
}

void initialiseParticlesRandomly(Global *g, Arena *arena) {
    GLUquadric *quadric = gluNewQuadric();
    const float maxVelocity = 1.0;
    float n[2], n_mag_sq, sum_radii, sum_radii_sq;
    bool collision, done;
    Particle *particles = g->particleCloud->particles;
    int numParticles = g->particleCloud->maxNumParticles;
    int debugCounter = 0;
    int i, j;

    for (i = 0; i < numParticles; i++) {
        particles[i].indexInCloud = i;

        particles[i].velocity.x = (random_uniform() - 0.5) * maxVelocity;
        particles[i].velocity.y = (random_uniform() - 0.5) * maxVelocity;
       
        particles[i].mass = random_uniform() * 0.1;
        if (&particles[i] == g->player || &particles[i] == g->enemy) {
            particles[i].mass *= 5;
        }
        particles[i].radius = getRadiusFromMass(particles[i].mass);
        particles[i].elasticity = 1.0;
        particles[i].quadric = quadric;
        particles[i].slices = 10;
        particles[i].loops = 3;
        done = false;
        float sideLength = sqrt(2) * arena->radius/2;

        debugCounter = 0;
        while (!done) {
            
            if (arena->isCircleArena) {
                particles[i].position.x = random_uniform() * (sideLength - (-sideLength) - 2.0 * particles[i].radius) + (-sideLength)  + particles[i].radius + epsilon;
                particles[i].position.y = random_uniform() * (sideLength - (-sideLength) - 2.0 * particles[i].radius) + (-sideLength)  + particles[i].radius + epsilon;
            } else {
                particles[i].position.x = random_uniform() * (arena->max.x - arena->min.x - 2.0 * particles[i].radius) + arena->min.x  + particles[i].radius + epsilon;
                particles[i].position.y = random_uniform() * (arena->max.y - arena->min.y - 2.0 * particles[i].radius) + arena->min.y  + particles[i].radius + epsilon;
            }

            /* Check for collision with existing particles. */
            collision = false;
            j = 0;
            while (!collision && j < i) {
                sum_radii = particles[i].radius + particles[j].radius;
                sum_radii_sq = sum_radii * sum_radii;
                n[0] = particles[j].position.x - particles[i].position.x;
                n[1] = particles[j].position.y - particles[i].position.y;
                n_mag_sq = n[0] * n[0] + n[1] * n[1];
                if (n_mag_sq < sum_radii_sq)
                    collision = true;
                else
                    j++;
            }
            if (!collision) 
                done = true;
        }
    }
}

void eulerStepSingleParticle(Particle &p, float dt) {
    p.position.x += dt * p.velocity.x;
    p.position.y += dt * p.velocity.y;
}

void integrateMotionParticles(ParticleCloud *particleCloud, float dt) {
    for (int i = 0; i < particleCloud->curNumParticles; i++) {
        eulerStepSingleParticle(particleCloud->particles[i], dt);
    }
}

void cleanUpDeadParticles(ParticleCloud *particleCloud) {
    int i;

    for (i = 2; i < particleCloud->curNumParticles; i++) {
        if (particleCloud->particles[i].isDead) {
            killParticleFromCloud(particleCloud, &particleCloud->particles[i]);
        }
    }
}

void updateParticles(ParticleCloud *particleCloud, Arena *arena, float dt) {

    /* Compute new positions of particles. */
    integrateMotionParticles(particleCloud, dt);

    /* Collisions against walls. */

    collideParticlesWall(particleCloud, arena);

    collideParticlesUniformGrid(particleCloud, arena, dt);

    cleanUpDeadParticles(particleCloud);
}


void displayParticle(Particle *p, float sx, float sy, float sz)
{
    if (p->radius <= 0.01) {
        return;
    }
    glPushMatrix();
    glScalef(sx, sy, sz);
    gluDisk(p->quadric, 0.0, p->radius, p->slices, p->loops);
    glPopMatrix();
}

void setColourParticle(Particle *p, float playerMass) {
    if (p->particleType == Player) {
        glColor3f(0.0, 0.6, 0.6);
    }
    else if (p->particleType == Enemy) {
        glColor3f(0.6, 0.6, 0.0);
    }
    else {
        glColor3f((p->mass) / (p->mass + playerMass),
            0,
            playerMass / (p->mass + playerMass) / 2);
    }
}

void displayParticles(Global *global, GLuint shader) {
    ParticleCloud *particleCloud = global->particleCloud;
    Particle *player = global->player;
    int i;

    glUseProgram(shader);
    for (i = 0; i < particleCloud->curNumParticles; i++) {
        glUniform1f(glGetUniformLocation(shader, "size"), particleCloud->particles[i].radius);
        glPushMatrix();
            
        setColourParticle(&particleCloud->particles[i], player->mass);

        glTranslatef(particleCloud->particles[i].position.x, particleCloud->particles[i].position.y, 0.0); 
        displayParticle(particleCloud->particles + i, 1.0, 1.0, 1.0);
        glPopMatrix();
    }
    
    glUseProgram(0);
}

int counter = 0;

void killParticleFromCloud(ParticleCloud *particleCloud, Particle *particle) {
    int particleIndex = 0;

    if (particle->particleType == Player || particle->particleType == Enemy) {
        return;
    }
    
    particleIndex = particle->indexInCloud;
    particleCloud->particles[particleIndex] = particleCloud->particles[particleCloud->curNumParticles - 1];
    particleCloud->particles[particleIndex].indexInCloud = particleIndex;
    particleCloud->curNumParticles--;
}

void moveParticleDirection(Particle *particle, glm::vec2 vec_dir, float dt) {
    glm::vec2 vec_dir_normalised;
    float magnitude;
    magnitude = sqrt((vec_dir.x * vec_dir.x) + (vec_dir.y * vec_dir.y));
    vec_dir_normalised = glm::normalize(vec_dir);

    particle->velocity += vec_dir_normalised * magnitude  * dt;
}

void moveParticle(Particle *particle, glm::vec2 worldPos, float dt) {
    glm::vec2 vec_dir;

    vec_dir = particle->position - worldPos;
    moveParticleDirection(particle, vec_dir, dt);
}

void moveEnemy(ParticleCloud *particleCloud, Particle *particle, float dt, float searchRange) {
    glm::vec2 finalDir = glm::vec2(0.0);
    Particle *particles = particleCloud->particles;
    int numParticles = particleCloud->curNumParticles;
    const float some_scalar = 5;
    int i;

    /* Simple steering behaviour. Move towards small targets*/
    for (i = 0; i < numParticles; i++) {
        if (particles[i].mass < particle->mass) {
            float dist = glm::distance(particle->position, particles[i].position);
            if (dist > searchRange / 2) {
                continue;
            }
            glm::vec2 dir = particles[i].position - particle->position;
            finalDir += dir / (dist);
        }
    }
    if (finalDir.x == 0 && finalDir.y == 0) return;
    finalDir = glm::normalize(finalDir) * some_scalar;
    moveParticleDirection(particle, finalDir, dt);
}



void blackhole(ParticleCloud *particleCloud, glm::vec2 world_vec, bool isGravity, float dt) {
    Particle *particles = particleCloud->particles;
    int maxParticles = particleCloud->curNumParticles;
    int i;

    for (i = 0; i < maxParticles; i++) {
        glm::vec2 vec_dir = world_vec - particles[i].position;

        float magnitude = sqrt((vec_dir.x * vec_dir.x) + (vec_dir.y * vec_dir.y));
        magnitude = 1 / magnitude;

        if (!isGravity) {
            magnitude *= -1;
        }

        particles[i].velocity += glm::normalize(vec_dir) * (magnitude * 10.0f) * dt;
    }
}

