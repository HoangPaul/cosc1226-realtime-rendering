
void main(void) {
    /* default vertex shader */
    gl_FrontColor = gl_Color;
    gl_Position =  gl_ModelViewProjectionMatrix * gl_Vertex;
}
