#ifndef H_MAIN
#define H_MAIN

#include "sdlbase.h"

struct Arena {
   glm::vec2 min;
   glm::vec2 max;
   glm::vec2 momentum;
   glm::vec2 centre;
   float radius;
   bool isCircleArena;
};

struct Global {
    struct ParticleCloud *particleCloud;
    Arena *arena;
    struct Particle *player;
    struct Particle *enemy;
};

typedef enum { MainMenu, Pause, Play, Win, Lose } GameState;

static const float milli = 1000.0;
const float epsilon = 1.0e-6;

void panic (const char *m);


#endif


