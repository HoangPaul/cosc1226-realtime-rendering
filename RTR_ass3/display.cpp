#include "main.h"
#include "arena.h"
#include "particle.h"

void showOSD(Time *time, int frameNo) {
    static const float interval = 1.0;
    static float frameRateInterval = 0.0;
    static int frameNoStartInterval = 0;
    static float elaspedTimeStartInterval = 0.0;
    static char buffer[80];
    float elapsedTime = time->elapsedTime;

    int len, i;

    if (elapsedTime < interval)
        return;

    if (elapsedTime > elaspedTimeStartInterval + interval) {
        frameRateInterval = (frameNo - frameNoStartInterval) /
            (elapsedTime - elaspedTimeStartInterval);
        elaspedTimeStartInterval = elapsedTime;
        frameNoStartInterval = frameNo;
    }


    //printf("displayOSD: frameNo %d elaspedTime %f frameRateInterval %f\n", frameNo, elaspedTime, frameRateInterval);
    glColor3f(1, 1, 1);
    sprintf(buffer, "framerate: %5d frametime: %5d", int(frameRateInterval), int(1.0 / frameRateInterval * 1000));

    /* render on screen */
    glPushMatrix();
    glLoadIdentity();
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glOrtho(0, 1, 1, 0, -1, 1);
    glMatrixMode(GL_MODELVIEW);

    glRasterPos2f(0, 1.0);
    len = (int)strlen(buffer);
    for (i = 0; i < len; i++) {
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15, buffer[i]);
    }

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
}

void showStateUI(GameState game_state) {
    static char buffer[80];
    int len, i;

    glPushAttrib(GL_ENABLE_BIT | GL_CURRENT_BIT);
    strcpy(buffer, "Empty");

    switch (game_state) {
    case MainMenu:
        strcpy(buffer, "Main Menu");
        glColor3f(1.0, 1.0, 1.0);
        break;
    case Play:
        strcpy(buffer, "Play");
        glColor3f(1.0, 1.0, 1.0);
        break;
    case Pause:
        strcpy(buffer, "Paused");
        glColor3f(0.0, 0.0, 1.0);
        break;
    case Win:
        glColor3f(0.0, 1.0, 0.0);
        strcpy(buffer, "You win!");
        break;
    case Lose:
        glColor3f(1.0, 0.0, 0.0);
        strcpy(buffer, "You lose!");
        break;
    }

    /* render on screen */
    glPushMatrix();
    glLoadIdentity();
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glOrtho(0, 1, 1, 0, -1, 1);
    glMatrixMode(GL_MODELVIEW);

    glRasterPos2f(0.45, 0.05);
    len = (int)strlen(buffer);
    for (i = 0; i < len; i++) {
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15, buffer[i]);
    }

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();

    glPopAttrib();
}


void displayMainMenu(Screen *screen) {
    glPushMatrix();
    glLoadIdentity();
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glOrtho(0, 1, 1, 0, -1, 1);
    glMatrixMode(GL_MODELVIEW);
    /*
    showStateUI(game_state);
    displayButtonUI(&testButton, windowWidth, windowHeight);
    */
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
}

void displayGameScene(Global *global, GLuint currShaderProgram, float zoom) {
    glPushMatrix();
    glOrtho(-zoom, zoom, -zoom, zoom, -10.0, 10.0);
    displayArena(global->arena);
    displayParticles(global, currShaderProgram);
    glPopMatrix();
}