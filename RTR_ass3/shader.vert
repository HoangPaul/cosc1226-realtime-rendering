uniform float size;

varying vec3 pos;

void main(void) {
    pos = gl_Vertex;
  
    /* default vertex shader */
    gl_FrontColor = gl_Color;
    gl_Position =  gl_ModelViewProjectionMatrix * gl_Vertex;
}
