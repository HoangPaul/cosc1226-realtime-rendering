float rand(vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}


void main (void)
{   
    vec4 colour = gl_Color;
    colour.x *= rand(gl_Color.xy);
    colour.y *= rand(gl_Color.yz);
    colour.z *= rand(gl_Color.zx);
    colour.w *= 1;
    
    gl_FragColor = colour;
}