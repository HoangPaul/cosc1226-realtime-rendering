#include "main.h"

void showOSD(Time *time, int frameNo);
void showStateUI(GameState game_state);
void displayMainMenu(SDL_Window *window, Time *time);
void displayGameScene(Global *global, GLuint currShaderProgram, float zoom);