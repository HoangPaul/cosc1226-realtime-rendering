#include "sdlbase.h"
#include "main.h"

/* Window */
Screen *screen;
const int defaultScreenWidth = 900;
const int defaultScreenHeight = 900;
const int defaultScreenZoom = 5.0;

/* Time */
Time *gameTime;

/* Mouse */
Input *input;



void timeTick(Time *time) {
    float dt = 0;

    time->elapsedTime = (SDL_GetTicks() / (float)1000.0) - time->startTime;

    dt = time->elapsedTime - time->lastTime;
    time->lastTime = time->elapsedTime;

    time->dt = dt <= 0 ? 0 : dt;
}

bool keyboard(SDL_Keycode key, bool isKeyDown) {
    switch (key) {
    case SDLK_q:
        return true;
    case SDLK_w:
        input->renderMode = !isKeyDown;
        break;
    case SDLK_p:
        system("pause");
        break;
    case SDLK_g:
        input->gravity = !isKeyDown;
        break;
    case SDLK_s:
        /*
        if (game_state == Pause) {
            game_state = Play;
            startTime = SDL_GetTicks() / (float)milli;
        } else {
            game_state = Pause;
        }
        */
        break;
    case SDLK_t:
        input->shader = !isKeyDown;
        break;
    case SDLK_r:
        input->restart = !isKeyDown;
        break;
    case SDLK_KP_MINUS:
        screen->zoom += 0.1;
        break;
    case SDLK_KP_PLUS:
        screen->zoom -= 0.1;
        break;
    }
    return false;
}

void mouse(unsigned int button, unsigned int state, int x, int y) {
    if (state == SDL_MOUSEBUTTONDOWN) {
        switch (button) {
        case SDL_BUTTON_LEFT:
            input->mouse.px_mouseX = x;
            input->mouse.px_mouseY = y;
            input->mouse.isLeftClick = true;
            break;
        case SDL_BUTTON_MIDDLE:
            break;
        case SDL_BUTTON_RIGHT:
            input->mouse.px_mouseX = x;
            input->mouse.px_mouseY = y;
            input->mouse.isRightClick = true;
            break;
        }
    } else if (state == SDL_MOUSEBUTTONUP) {
        switch (button) {
        case SDL_BUTTON_LEFT:
            input->mouse.px_mouseX = x;
            input->mouse.px_mouseY = y;
            input->mouse.isLeftClick = false;
            break;
        case SDL_BUTTON_MIDDLE:
            break;
        case SDL_BUTTON_RIGHT:
            input->mouse.px_mouseX = x;
            input->mouse.px_mouseY = y;
            input->mouse.isRightClick = false;
            break;
        }
    }
}

void motion(int x, int y) {
    input->mouse.px_mouseX = x;
    input->mouse.px_mouseY = y;
}

bool handleEvents() {
    SDL_Event event;

    while (SDL_PollEvent(&event)) {
        switch (event.type) {
        case SDL_MOUSEMOTION:
            motion(event.motion.x, event.motion.y);
            break;
        case SDL_MOUSEBUTTONDOWN:
            mouse(event.button.button, SDL_MOUSEBUTTONDOWN, event.button.x, event.button.y);
            break;
        case SDL_MOUSEBUTTONUP:
            mouse(event.button.button, SDL_MOUSEBUTTONUP, event.button.x, event.button.y);
            break;
        case SDL_KEYDOWN:
            if (keyboard(event.key.keysym.sym, true)) {
                return true;
            }
            break;
        case SDL_KEYUP:
            if (keyboard(event.key.keysym.sym, false)) {
                return true;
            }
            break;
        case SDL_WINDOWEVENT:
            switch (event.window.event) {
            case SDL_WINDOWEVENT_RESIZED:
                if (event.window.windowID == SDL_GetWindowID(screen->mainWindow)) {
                    SDL_SetWindowSize(screen->mainWindow, event.window.data1, event.window.data2);
                    reshape(screen, event.window.data1, event.window.data2);
                }
                break;
            case SDL_WINDOWEVENT_CLOSE:
                return true;
                break;
            default:
                break;
            }
            break;
        default:
            break;
        }
    }

    return false;
}

void resetInput(Input *input) {
    input->gravity = false;
    input->shader = false;
    input->restart = false;
    input->renderMode = false;
}

void mainloop() {
    bool done = false;
    while (!done) {
        if (handleEvents()) {
            done = true;
            continue;
        }
        timeTick(gameTime);
        update(screen, input, gameTime);
        display(screen, gameTime);
        resetInput(input);
    }
    cleanup();
}

void initSDLVariables() {
    gameTime = (Time*)calloc(1, sizeof(Time));
    input = (Input*)calloc(1, sizeof(Input));
    screen = (Screen*)calloc(1, sizeof(Screen));
    screen->windowHeight = defaultScreenHeight;
    screen->windowWidth = defaultScreenWidth;
    screen->zoom = defaultScreenZoom;
}

void initGraphics() {
    SDL_GLContext mainGLContext;

    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);

    screen->mainWindow = SDL_CreateWindow("Replace this text", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, screen->windowWidth, screen->windowHeight, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);

    if (screen->mainWindow == NULL) {
        fprintf(stderr, "Failed to create window: %s\n", SDL_GetError());
        exit(EXIT_SUCCESS);
    }

    mainGLContext = SDL_GL_CreateContext(screen->mainWindow);
    SDL_GL_MakeCurrent(screen->mainWindow, mainGLContext);
}

int main(int argc, char** argv) {
    glutInit(&argc, argv);

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        fprintf(stderr, "Unable to init SDL: %s\n", SDL_GetError());
        return EXIT_SUCCESS;
    }

    initSDLVariables();

    initGraphics();

#if _WIN32
    GLenum err = glewInit();

    if (GLEW_OK != err) {
        printf("%s\n", glewGetErrorString(err));
        char c;
        scanf("%d", &c);
        exit(1);
    }
#endif
    
    reshape(screen, defaultScreenWidth, defaultScreenHeight);
    
    myInit();

    mainloop();

    return EXIT_SUCCESS;
}
