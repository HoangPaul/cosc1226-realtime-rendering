#ifndef H_ARENA
#define H_ARENA

#include "main.h"
#include "particle.h"

void collideParticlesWall(ParticleCloud *particleCloud, Arena *arena);
void initArena(Global *g, int size, bool isCircleArena);
void cleanupArena(Global *global);
void displayArena(Arena *arena);

#endif